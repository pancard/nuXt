<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>			
<div id="page">
	<h1>Pages</h1>
	<h2>Vos Pages</h2>
	<div class="bloc_recherche">
			<form>
				<label for="recherche">Recherche </label><input type="text" id="recherche" name="recherche" value=""  class="text">
				<div class="bouton">
					<input type="reset" value="Effacer" class="reset">
					<input type="button" value="Rechercher" class="submit" onClick="jsAfficherPagesWCritere(document.getElementById('recherche').value);">
				</div>
			</form>
	</div>
	<div class="bloc_ajout">
		<input type="submit" value="Créer" class="submit" id="openerCreatGd">
	</div>
<!--
	<div id="lbl_resultat"></div>
	-->
		<div class="resultat">
			<div id="dialogCreatGd" title="Création">
				<form>
					<label for="addtitre" id="lbl_titre" class="obligatoire">Titre </label><input type="text" id="addtitre" name="addtitre" class="text" /><br>
					<br>
					<label for="addtexte" id="lbl_texte">Texte </label>
						<textarea name="addtexte" id="addtexte" class="area" cols="100" rows="10"></textarea><br>
					<br>
					<input type="button" value="Annuler" class="reset" id="closeCreat">
					<input type="button" value="Créer" class="submit" onClick="jsCreerPage(document.getElementById('addtitre').value,document.getElementById('addtexte').value);">
				</form>
			</div>
			<div id="dialogModifGd" title="Modification">
				<form>
					<label for="upid" id="lbl_id" > </label><input type="hidden" name="upid" id="upid" />
					<label for="upmid" id="lbl_mid" > </label><input type="hidden" name="upmid" id="upmid" /><br>
					<label for="uptitre" id="lbl_titre" class="obligatoire">Titre </label><input type="text" id="uptitre" name="uptitre" class="text" /><br>
					<br>
					<label for="uptexte" id="lbl_texte">Texte </label>
						<textarea name="uptexte" id="uptexte" class="area" cols="100" rows="10"></textarea><br>
					<br>
					<input type="button" value="Annuler" class="reset" id="closeModif">
					<input type="button" value="Modifier" class="submit" onClick="jsModifierPage(document.getElementById('upid').value,document.getElementById('upmid').value,document.getElementById('uptitre').value,document.getElementById('uptexte').value);">					
				</form>
			</div>
			
			<table id="tablePage">
			<script type="text/javascript">
        		xajax_xAfficherPages();//On appelle la fonction refresh() pour lancer le script.
            </script>
			</table>
			</div>
	
</div>

<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>