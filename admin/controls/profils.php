<?php
function xAfficherProfils() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tableProfil','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Libellé</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_profils WHERE profils_valide = 1 AND profils_id > 0 ORDER BY profils_libelle;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['profils_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverProfil('.$value['profils_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverProfil('.$value['profils_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['profils_id'].'</td>';
			$tableau .= '<td>'.$value['profils_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherProfilById('.$value['profils_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableProfil','innerHTML',$tableau);
        }
		
		return $reponse;
    }
    
function xAfficherProfilById($id) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_profils WHERE profils_id = '.$id.' AND profils_id > 0 ;');
		deconnexion();
			
		while($ligne = mysql_fetch_array($repSql)) {
			$reponse->assign('upid', 'value', $ligne['profils_id']);
			$reponse->assign('uplibelle', 'value', $ligne['profils_libelle']);
			$reponse->assign('upListeDroits','innerHTML', getInputDroits($id));
		}
			
		return $reponse;
}

function xCreerProfil($libelle,$d) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableProfil','innerHTML');
		$reponse->clear('addListeDroits','innerHTML');
		$reponse->assign('addListeDroits', 'innerHTML', getInputDroits(""));	
		
		$nextId = getNextID('profils');
			
		connexion();
		$libelle = securite_bdd($libelle);
		
		// Je retravaille les droits et leurs valeurs ex: 1-1;2-0 = id1-actif;id2-non actif
		$droits = explode(";",$d);
		for($i = 0; $i < count($droits); $i++) {
		    //$people[$i]['salt'] = mt_rand(000000, 999999);
		    $droit = explode("-",$droits[$i]);
		    sql('INSERT INTO nuxt_lien_droits_profils (l_profils_id,l_droits_id,l_actif)
		    			VALUES ("'.$nextId.'","'.$droit[0].'","'.$droit[1].'");');
		}
			
		sql('INSERT INTO nuxt_profils (profils_id,profils_libelle,profils_valide) 
					VALUES ("'.$nextId.'","'.$libelle.'","1");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Profil '.$libelle.' crée avec succès !');
		$reponse->call('xajax_xAfficherProfils');
		
		return $reponse;
	}

function xModifierProfil($id,$libelle,$d) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableProfil','innerHTML');	
		$reponse->clear('upListeDroits','innerHTML');
		$reponse->assign('upListeDroits', 'innerHTML', getInputDroits($id));
		connexion();
			
		$libelle = securite_bdd($libelle);	
			
		// Je retravaille les droits et leurs valeurs ex: 1-1;2-0 = id1-actif;id2-non actif
		$droits = explode(";",$d);
		for($i = 0; $i < count($droits); $i++) {
		    //$people[$i]['salt'] = mt_rand(000000, 999999);
		    $droit = explode("-",$droits[$i]);
		    sql('UPDATE nuxt_lien_droits_profils SET l_actif = "'.$droit[1].'" WHERE l_droits_id = "'.$droit[0].'" AND l_profils_id = "'.$id.'";');
		}	
		
		sql('UPDATE nuxt_profils SET profils_libelle = "'.$libelle.'" WHERE profils_id = "'.$id.'";');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Profil '.$libelle.' modifié avec succès !');
		$reponse->call('xajax_xAfficherProfils');
		
		return $reponse;
	}

function xArchiverProfil($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tableProfil','innerHTML');
		connexion();
		
		sql('UPDATE nuxt_profils SET profils_valide = '.$value.' WHERE profils_id = '.$id.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Désactivation ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg.' du profil effectuée avec succès';
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->call('xajax_xAfficherProfils');
		
		return $reponse;
		
	}
	
function xAfficherProfilsWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tableProfil','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " (profils_libelle like '%".$crit."%') ";
    		$where .= " AND profils_id > 0 ";
    	}

    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Libellé</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_profils ".$where."  ORDER BY profils_libelle;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['profils_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverProfil('.$value['profils_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverProfil('.$value['profils_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['profils_id'].'</td>';
			$tableau .= '<td>'.$value['profils_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherProfilById('.$value['profils_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableProfil','innerHTML',$tableau);
        }
		return $reponse;
    }
    
function getInputDroits($id) {
	$res = "<br>";

	if($id!="") {
		connexion();
		$sql = sql("SELECT *
				FROM nuxt_lien_droits_profils
				INNER JOIN nuxt_droits ON droits_id = l_droits_id
				WHERE droits_valide = 1
				AND l_profils_id = ".$id."
				ORDER BY droits_libelle;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['l_actif']==1) { $ch = "checked";} else { $ch = "";}
			$res .= "<input type='checkbox' name='droits[]' id='d".$value['droits_id']."' value='".$value['droits_id']."' ".$ch." />&nbsp;".lireBdd($value['droits_libelle'],false)."<br>";
	    }
	}
	else {
		connexion();
		$sql = sql("SELECT * FROM nuxt_droits WHERE droits_valide = 1 ORDER BY droits_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			$res .= "<input type='checkbox' name='droits[]' id='d".$value['droits_id']."' value='".$value['droits_id']."' />&nbsp;".lireBdd($value['droits_libelle'],false)."<br>";
	    }
	}
 
    return $res;
}
?>