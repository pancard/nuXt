<?php
function xAfficherUsers() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tableUser','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
					<th>Avatar</th>
					<th>Login</th>
					<th>Mail</th>
					<!--<th>Téléphone</th>
					<th>Portable</th>-->
					<th>Profil</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_users INNER JOIN nuxt_profils ON profils_id = users_profil_id WHERE users_valide = 1 AND users_id > 0 ORDER BY users_login;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['users_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverUser('.$value['users_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverUser('.$value['users_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td><img src="'.$GLOBALS['avatarChemin'].$value['users_avatar'].'" alt="Avatar de '.$value['users_login'].'" class="icone"/></td>';
			$tableau .= '<td>'.$value['users_login'].'</td>';
			$tableau .= '<td>'.$value['users_mail'].'</td>';
			/*$tableau .= '<td>'.$value['users_fixe'].'</td>';
			$tableau .= '<td>'.$value['users_portable'].'</td>';*/
			$tableau .= '<td>'.$value['profils_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherUserById('.$value['users_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableUser','innerHTML',$tableau);
        }
		
		return $reponse;
    }
    
function xAfficherUserById($id) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_users WHERE users_id = '.$id.' AND users_id > 0 ;');
		deconnexion();
			
		while($ligne = mysql_fetch_array($repSql)) {
			$reponse->assign('upid', 'value', $ligne['users_id']);
			$reponse->assign('uplogin', 'value', $ligne['users_login']);
			$reponse->assign('uppassword', 'value', $ligne['users_password']);
			$reponse->assign('upmail', 'value', $ligne['users_mail']);
			$reponse->assign('upListeProfils', 'innerHTML', getSelectProfils($id));	
		}
			
		return $reponse;
}

function xCreerUser($login,$password,$mail,$profil) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableUser','innerHTML');
		$reponse->clear('addListeProfils','innerHTML');
		$reponse->assign('addListeProfils', 'innerHTML', getSelectProfils(""));	
		
		$nextId = getNextID('users');
		
		connexion();
				
		$login = securite_bdd($login);
		$password = securite_bdd($password);
		$mail = securite_bdd($mail);	
			
		sql('INSERT INTO nuxt_users (users_id, users_login, users_password, users_mail, users_valide, users_profil_id) 
					VALUES ("'.$nextId.'", "'.$login.'", "'.$password.'", "'.$mail.'","1", "'.$profil.'");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Utilisateur '.$login.' crée avec succès !');
		$reponse->call('xajax_xAfficherUsers');
		
		return $reponse;
	}

function xModifierUser($id,$login,$password,$mail,$profil) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableUser','innerHTML');
		$reponse->clear('upListeProfils','innerHTML');
		$reponse->assign('upListeProfils', 'innerHTML', getSelectProfils($id));	
		connexion();
			
		$login = securite_bdd($login);
		$password = securite_bdd($password);
		$mail = securite_bdd($mail);	
			
		sql('UPDATE nuxt_users SET users_login = "'.$login.'", users_password = "'.$password.'", users_mail = "'.$mail.'", users_profil_id = "'.$profil.'" WHERE users_id = "'.$id.'";');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Utilisateur '.$login.' modifié avec succès !');
		$reponse->call('xajax_xAfficherUsers');
		
		return $reponse;
	}

function xArchiverUser($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tableUser','innerHTML');
		connexion();
		
		sql('UPDATE nuxt_users SET users_valide = '.$value.' WHERE users_id = '.$id.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Désactivation ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg.' du compte effectuée avec succès';
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->call('xajax_xAfficherUsers');
		
		return $reponse;
		
	}
	
function xAfficherUsersWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tableUser','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " (users_login like '%".$crit."%' ";
    		$where .= " OR users_mail like '%".$crit."%' ";
    		$where .= " OR users_fixe like '%".$crit."%' ";
    		$where .= " OR users_portable like '%".$crit."%' ";
    		$where .= " OR users_facebook like '%".$crit."%' ";
    		$where .= " OR users_twitter like '%".$crit."%')  AND users_id > 0 ";
    	}

    	$tableau = "";
    	$tableau .= "<tr>
					<th>Avatar</th>
					<th>Login</th>
					<th>Mail</th>
					<!--<th>Téléphone</th>
					<th>Portable</th>-->
					<th>Profil</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_users INNER JOIN nuxt_profils ON profils_id = users_profil_id ".$where."  ORDER BY users_login;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['users_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverUser('.$value['users_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverUser('.$value['users_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td><img src="'.$GLOBALS['avatarChemin'].$value['users_avatar'].'" alt="Avatar de '.$value['users_login'].'" class="icone"/></td>';
			$tableau .= '<td>'.$value['users_login'].'</td>';
			$tableau .= '<td>'.$value['users_mail'].'</td>';
			/*$tableau .= '<td>'.$value['users_fixe'].'</td>';
			$tableau .= '<td>'.$value['users_portable'].'</td>';*/
			$tableau .= '<td>'.$value['profils_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherUserById('.$value['users_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableUser','innerHTML',$tableau);
        }
		return $reponse;
    }
    
function getDroitsUsers($id) {
	connexion();
	$sql = sql("SELECT l_droits_id, l_actif
				FROM nuxt_lien_droits_profils
				INNER JOIN nuxt_profils ON profils_id = l_profils_id
				INNER JOIN nuxt_users ON users_profil_id = profils_id
				WHERE users_id = ".$id."; ");
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {
		$res[$ligne['l_droits_id']] = $ligne['l_actif'];
	}
	
	return $res;
}

function getUserProfilID($id) {
	connexion();
	$sql = sql("SELECT users_profil_id FROM nuxt_users WHERE users_id = '".$id."';");
	deconnexion();
	
	$res = 0;
	
	while($ligne = mysql_fetch_array($sql)) {
		$res = $ligne['users_profil_id'];
	}
	
	return $res;
}

function getSelectProfils($id) {
	$res = "";

	if($id!="") {
		// je vais chercher l'id du profil du user sur lequel je bosse
		$idProfil = getUserProfilID($id);
		$res .= "<select name='upprofil' id='upprofil'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_profils WHERE profils_valide = 1 AND profils_id > 0 ORDER BY profils_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['profils_id']==$idProfil) { $se = "selected";} else { $se = "";}
			$res .= "<option value='".$value['profils_id']."' ".$se." />".lireBdd($value['profils_libelle'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
	else {
		$res .= "<select name='addprofil' id='addprofil'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_profils WHERE profils_valide = 1 AND profils_id > 0 ORDER BY profils_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			$res .= "<option value='".$value['profils_id']."' />".lireBdd($value['profils_libelle'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
 
    return $res;
}
?>