<?php
function xAfficherCategories() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tableCategorie','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Libellé</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_articles_categories WHERE articles_categories_valide = 1 AND articles_categories_id > 0 ORDER BY articles_categories_libelle;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['articles_categories_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverCategorie('.$value['articles_categories_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverCategorie('.$value['articles_categories_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['articles_categories_id'].'</td>';
			$tableau .= '<td>'.$value['articles_categories_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherCategorieById('.$value['articles_categories_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableCategorie','innerHTML',$tableau);
        }
		
		return $reponse;
    }
    
function xAfficherCategorieById($id) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_articles_categories WHERE articles_categories_id = '.$id.' AND articles_categories_id > 0 ;');
		deconnexion();
			
		while($ligne = mysql_fetch_array($repSql)) {
			$reponse->assign('upid', 'value', $ligne['articles_categories_id']);
			$reponse->assign('uplibelle', 'value', $ligne['articles_categories_libelle']);
		}
			
		return $reponse;
}

function xCreerCategorie($libelle) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableCategorie','innerHTML');	
		
		$nextID = getNextID('articles_categories');
		
		connexion();
			
		$libelle = securite_bdd($libelle);	
			
		sql('INSERT INTO nuxt_articles_categories (articles_categories_id,articles_categories_libelle,articles_categories_valide) 
					VALUES ("'.$nextID.'", "'.$libelle.'","1");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Categorie '.$libelle.' crée avec succès !');
		$reponse->call('xajax_xAfficherCategories');
		
		return $reponse;
	}

function xModifierCategorie($id,$libelle) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableCategorie','innerHTML');	
		connexion();
			
		$libelle = securite_bdd($libelle);	
			
		sql('UPDATE nuxt_articles_categories SET articles_categories_libelle = "'.$libelle.'" WHERE articles_categories_id = "'.$id.'";');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Categorie '.$libelle.' modifié avec succès !');
		$reponse->call('xajax_xAfficherCategories');
		
		return $reponse;
	}

function xArchiverCategorie($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tableCategorie','innerHTML');
		connexion();
		
		sql('UPDATE nuxt_articles_categories SET articles_categories_valide = '.$value.' WHERE articles_categories_id = '.$id.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Désactivation ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg.' de la categorie effectuée avec succès';
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->call('xajax_xAfficherCategories');
		
		return $reponse;
		
	}
	
function xAfficherCategoriesWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tableCategorie','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " (articles_categories_libelle like '%".$crit."%') ";
    		$where .= " AND articles_categories_id > 0 ";
    	}

    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Libellé</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_articles_categories ".$where." ORDER BY articles_categories_libelle;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['articles_categories_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverCategorie('.$value['articles_categories_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverCategorie('.$value['articles_categories_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['articles_categories_id'].'</td>';
			$tableau .= '<td>'.$value['articles_categories_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherCategorieById('.$value['articles_categories_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableCategorie','innerHTML',$tableau);
        }
		return $reponse;
    }
?>