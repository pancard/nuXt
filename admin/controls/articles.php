<?php
function xAfficherArticles() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tableArticle','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Catégorie</th>
					<th>Titre</th>
					<th>Créateur</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_articles art 
			INNER JOIN nuxt_articles_categories cat ON cat.articles_categories_id = art.articles_categories_id 
			INNER JOIN nuxt_users ON users_id = articles_users_id_createur 
			WHERE articles_valide = 1 ORDER BY articles_date_creation DESC;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['articles_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverArticle('.$value['articles_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverArticle('.$value['articles_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['articles_id'].'</td>';
			$tableau .= '<td>'.$value['articles_categories_libelle'].'</td>';
			$tableau .= '<td>'.$value['articles_titre'].'</td>';
			$tableau .= '<td>'.$value['users_login'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherArticleById('.$value['articles_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableArticle','innerHTML',$tableau);
        }
		
		return $reponse;
    }
    
function xAfficherArticleById($id) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_articles WHERE articles_id = '.$id.' ;');
		deconnexion();
			
		while($ligne = mysql_fetch_array($repSql)) {
			$reponse->assign('upid', 'value', $ligne['articles_id']);
			$reponse->assign('uptitre', 'value', lireBdd($ligne['articles_titre'],true));
			$reponse->assign('uptexte', 'value', lireBdd($ligne['articles_texte'],true));
			$reponse->assign('uptags', 'value', lireBdd($ligne['articles_tags'],true));
			$reponse->assign('upimg', 'value', lireBdd($ligne['articles_image'],true));
			$reponse->assign('upListeCategories', 'innerHTML', getSelectCategories($id));	
			$reponse->assign('upListeCreateur', 'innerHTML', getSelectCreateur($id));	
		}
			
		return $reponse;
}

function xAfficheOptions() {
	$reponse = new xajaxResponse();	
		
	connexion();
	$sql = sql('SELECT * FROM nuxt_articles WHERE articles_valide = 1 ORDER BY articles_titre ;');
	deconnexion();
	
	$idUp = 0;
	while($ligne = mysql_fetch_array($sql)) {
		if($ligne['articles_upload_id']!=0) {
			$idUp = $ligne['articles_upload_id'];
		}
	}
	
	$reponse->assign('listeArticles', 'innerHTML', getSelectArticles(0));
	$reponse->assign('listeUpload', 'innerHTML', getSelectUpload($idUp));
	
	
	return $reponse;
}

function xOptions($art,$img) {
	$reponse = new xajaxResponse();
	
	connexion();
	sql("UPDATE nuxt_articles SET articles_miseenavant = 0;");
	sql("UPDATE nuxt_articles SET articles_miseenavant = 1, articles_upload_id = ".$img." WHERE articles_id = ".$art);
	deconnexion();
	
	return $reponse;
}

function xCreerArticle($titre,$texte,$categorie,$createur,$tags,$img) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableArticle','innerHTML');
		$reponse->clear('addListeCategories','innerHTML');
		$reponse->clear('addListeCreateur','innerHTML');
		$reponse->assign('addListeCategories', 'innerHTML', getSelectCategories(""));	
		$reponse->assign('addListeCreateur', 'innerHTML', getSelectCreateur(""));	
		
		$nextID = getNextID('articles');
		
		connexion();
			
		$titre = securite_bdd($titre);
		$texte = securite_bdd($texte);
		$createur = securite_bdd($createur);
		$categorie = securite_bdd($categorie);
		$tags = securite_bdd($tags);

		$date = date('Y-m-d');	
			
		sql('INSERT INTO nuxt_articles (articles_id,articles_titre,articles_texte,articles_categories_id,articles_users_id_createur,articles_date_creation,articles_tags,articles_image,articles_valide) 
					VALUES ("'.$nextID.'","'.$titre.'","'.$texte.'","'.$categorie.'","'.$createur.'","'.$date.'","'.$tags.'","'.$img.'","1");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Article '.$titre.' crée avec succès !');
		$reponse->call('xajax_xAfficherArticles');
		
		return $reponse;
	}

function xModifierArticle($id,$titre,$texte,$categorie,$createur,$tags,$img) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableArticle','innerHTML');
		$reponse->clear('upListeCategories','innerHTML');
		$reponse->clear('upListeCreateur','innerHTML');
		$reponse->assign('upListeCategories', 'innerHTML', getSelectCategories(""));
		$reponse->assign('upListeCreateur', 'innerHTML', getSelectCreateur(""));	
		connexion();
			
		$titre = securite_bdd($titre);
		$texte = securite_bdd($texte);
		$createur = securite_bdd($createur);
		$categorie = securite_bdd($categorie);
		$tags = securite_bdd($tags);

		$dateMaj = date('Y-m-d');
			
		sql('UPDATE nuxt_articles SET articles_titre = "'.$titre.'", articles_texte = "'.$texte.'", articles_categories_id = "'.$categorie.'", articles_users_id_createur = "'.$createur.'"
			, articles_date_modification = "'.$dateMaj.'", articles_tags = "'.$tags.'", articles_image = "'.$img.'" WHERE articles_id = "'.$id.'";');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Article '.$titre.' modifié avec succès !');
		$reponse->call('xajax_xAfficherArticles');
		
		return $reponse;
	}

function xArchiverArticle($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tableArticle','innerHTML');
		connexion();
		
		sql('UPDATE nuxt_articles SET articles_valide = '.$value.' WHERE articles_id = '.$id.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Désactivation ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg." de l'article effectuée avec succès";
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->call('xajax_xAfficherArticles');
		
		return $reponse;
		
	}
	
function xAfficherArticlesWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tableArticle','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	$jointure = " INNER JOIN nuxt_users ON users_id = art.articles_users_id_createur ";
    	$jointure .= " INNER JOIN nuxt_articles_categories cat ON cat.articles_categories_id = art.articles_categories_id ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " art.articles_titre like '%".$crit."%' ";
    		$where .= " OR art.articles_texte like '%".$crit."%' ";
    		$where .= " OR users_login like '%".$crit."%' ";
    		$where .= " OR cat.articles_categories_libelle like '%".$crit."%' ";
    		$where .= " OR art.articles_tags like '%".$crit."%' ";
    	}
    	
    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Catégorie</th>
					<th>Titre</th>
					<th>Créateur</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
				
		// Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_articles art ".$jointure." ".$where." ORDER BY articles_date_creation DESC;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['articles_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverArticle('.$value['articles_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverArticle('.$value['articles_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['articles_id'].'</td>';
			$tableau .= '<td>'.lireBdd($value['articles_categories_libelle'],false).'</td>';
			$tableau .= '<td>'.lireBdd($value['articles_titre'],false).'</td>';
			$tableau .= '<td>'.lireBdd($value['users_login'],false).'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherArticleById('.$value['articles_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableArticle','innerHTML',$tableau);
        }
		return $reponse;
    }
    
function getCategoriesSelect($idASelectionner) {

	connexion();
	$sql = sql("SELECT * FROM nuxt_articles_categories WHERE articles_categories_valide = 1 ORDER BY articles_categories_libelle");
	deconnexion();

	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<option value='".$ligne['articles_categories_id']."'";
		if ($idASelectionner==$ligne['articles_categories_id']) {
			$res.= " selected";
		}
		$res .= ">".lireBdd($ligne['articles_categories_libelle'],false)."</option>";
	}
	
	return $res;
}

function getUsersSelect($idASelectionner) {

	connexion();
	$sql = sql("SELECT * FROM nuxt_users WHERE users_valide = 1 AND users_profil_id >= 0 ORDER BY users_login");
	deconnexion();

	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<option value='".$ligne['users_id']."'";
		if ($idASelectionner==$ligne['users_id']) {
			$res.= " selected";
		}
		$res .= ">".lireBdd($ligne['users_login'],false)."</option>";
	}
	
	return $res;
}

function getArticleCategorieID($id) {
	connexion();
	$sql = sql("SELECT articles_categories_id FROM nuxt_articles WHERE articles_id = '".$id."';");
	deconnexion();
	
	$res = 0;
	
	while($ligne = mysql_fetch_array($sql)) {
		$res = $ligne['articles_categories_id'];
	}
	
	return $res;
}

function getArticleUserID($id) {
	connexion();
	$sql = sql("SELECT articles_users_id_createur FROM nuxt_articles WHERE articles_id = '".$id."';");
	deconnexion();
	
	$res = 0;
	
	while($ligne = mysql_fetch_array($sql)) {
		$res = $ligne['articles_users_id_createur'];
	}
	
	return $res;
}

function getSelectCategories($id) {
	$res = "";

	if($id!="") {
		// je vais chercher l'id du profil du user sur lequel je bosse
		$idCategorie = getArticleCategorieID($id);
		$res .= "<select name='upcategorie' id='upcategorie'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_articles_categories WHERE articles_categories_valide = 1 ORDER BY articles_categories_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['articles_categories_id']==$idCategorie) { $se = "selected";} else { $se = "";}
			$res .= "<option value='".$value['articles_categories_id']."' ".$se." />".lireBdd($value['articles_categories_libelle'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
	else {
		$res .= "<select name='addcategorie' id='addcategorie'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_articles_categories WHERE articles_categories_valide = 1 ORDER BY articles_categories_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			$res .= "<option value='".$value['articles_categories_id']."' />".lireBdd($value['articles_categories_libelle'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
 
    return $res;
}

function getSelectCreateur($id) {
	$res = "";

	if($id!="") {
		// je vais chercher l'id du profil du user sur lequel je bosse
		$idUser = getArticleUserID($id);
		$res .= "<select name='upcreateur' id='upcreateur'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_users WHERE users_valide = 1 AND users_id > 0 ORDER BY users_login; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['users_id']==$idUser) { $se = "selected";} else { $se = "";}
			$res .= "<option value='".$value['users_id']."' ".$se." />".lireBdd($value['users_login'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
	else {
		$res .= "<select name='addcreateur' id='addcreateur'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_users WHERE users_valide = 1 AND users_id > 0 ORDER BY users_login; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			$res .= "<option value='".$value['users_id']."' />".lireBdd($value['users_login'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
 
    return $res;
}

function getSelectArticles($idASelectionner) {

	$res = '<select id="art" name="art"><option value="0">Par défaut</option>';

	connexion();
	$sql = sql("SELECT * FROM nuxt_articles WHERE articles_valide = 1 ORDER BY articles_titre;");
	deconnexion();

	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<option value='".$ligne['articles_id']."'";
		if($ligne['articles_miseenavant']==1) {
			$res.= " selected";
		}
		$res .= ">".$ligne['articles_titre']."</option>";
	}
	
	$res .= "</select>";
	
	return $res;
}
?>