<?php
function creerUpload($nom,$chemin,$type,$taille) {
	$nextID = getNextID('upload');
	
	connexion();
	$sql = sql("INSERT INTO ".$GLOBALS['prefixe']."upload (upload_id,upload_nom,upload_chemin,upload_type,upload_taille,upload_valide) 
					VALUES ('".$nextID."','".$nom."','".$chemin."','".$type."','".$taille."','1');");
	deconnexion();
}

function xAfficherUpload() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tableUpload','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
    				<th>ID</th>
					<th>Image</th>
					<th>Nom</th>
					<th>Type</th>
					<th>Taille</th>
					<th>Supprimer</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_upload WHERE upload_valide = 1 ORDER BY upload_nom;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['upload_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Actif" class="icone" onClick="jsArchiverUpload('.$value['upload_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Inactif" class="icone"  onClick="jsArchiverUpload('.$value['upload_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['upload_id'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['uploadChemin'].$value['upload_chemin'].'" alt="Image : '.$value['upload_nom'].'" class="icone"/></td>';
			$tableau .= '<td>'.$value['upload_nom'].'</td>';
			$tableau .= '<td>'.$value['upload_type'].'</td>';
			$tableau .= '<td>'.(round($value['upload_taille'] / 1048576 * 100) / 100).' Mo</td>';
			/*$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherUploadById('.$value['upload_id'].');" /></td>';*/
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableUpload','innerHTML',$tableau);
        }
        
        $reponse->assign('ftp','innerHTML',getSizeRepFtp());
		
		return $reponse;
    }

function xCreerUpload($nom,$file) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableUpload','innerHTML');
		
		$nextId = getNextID('upload');
		
		connexion();
				
		$nom = securite_bdd($nom);
		$chemin = $GLOBALS['uploadChemin'].$file['name'];
		$type = $file['type'];
		$taille = $file['size'];
			
		sql('INSERT INTO nuxt_upload (upload_id, upload_nom, upload_chemin, upload_type, upload_taille, upload_valide) 
					VALUES ("'.$nextId.'", "'.$nom.'", "'.$chemin.'", "'.$type.'","'.$taille.'", "1");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Upload du fichier terminé');
		$reponse->assign('ftp','innerHTML',getSizeRepFtp());
		$reponse->call('xajax_xAfficherUpload');
		
		return $reponse;
	}

function xArchiverUpload($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tableUpload','innerHTML');
		connexion();
		// je récupère le chemin pour supprimer l'image du ftp
		$sql = sql("SELECT upload_chemin FROM nuxt_upload WHERE upload_id = ".$id);
		while($ligne = mysql_fetch_array($sql)) {
			$chemin = $ligne['upload_chemin'];
		}
		unlink($GLOBALS['uploadChemin'].$chemin);
		
		sql('DELETE FROM nuxt_upload WHERE upload_id = '.$id.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Suppression ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg." de l'image effectuée avec succès";
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->assign('ftp','innerHTML',getSizeRepFtp());
		$reponse->call('xajax_xAfficherUpload');
		
		return $reponse;
		
	}
	
function xAfficherUploadWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tableUpload','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " (upload_nom like '%".$crit."%' ";
    		$where .= " OR upload_type like '%".$crit."%' ";
    		$where .= " OR upload_taille like '%".$crit."%') ";
    	}

    	$tableau = "";
    	$tableau .= "<tr>
    				<th>ID</th>
					<th>Image</th>
					<th>Nom</th>
					<th>Type</th>
					<th>Taille</th>
					<th>Supprimer</th>
				</tr>";
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_upload ".$where."  ORDER BY upload_nom;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['upload_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Actif" class="icone" onClick="jsArchiverUpload('.$value['upload_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Inactif" class="icone"  onClick="jsArchiverUpload('.$value['upload_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['upload_id'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['uploadChemin'].$value['upload_chemin'].'" alt="Image : '.$value['upload_nom'].'" class="icone"/></td>';
			$tableau .= '<td>'.$value['upload_nom'].'</td>';
			$tableau .= '<td>'.$value['upload_type'].'</td>';
			$tableau .= '<td>'.(round($value['upload_taille'] / 1048576 * 100) / 100).' Mo</td>';
			/*$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherUploadById('.$value['upload_id'].');" /></td>';*/
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableUpload','innerHTML',$tableau);
        }
        
        $reponse->assign('ftp','innerHTML',getSizeRepFtp());
        
		return $reponse;
    }
    
?>