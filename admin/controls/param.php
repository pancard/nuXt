<?
function getSelectParam($idASelectionner) {

	$res = '<select id="idDesignSite" name="idDesignSite">';

	connexion();
	$sql = sql("SELECT * FROM nuxt_design WHERE design_valide = 1 ORDER BY design_nom;");
	deconnexion();

	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<option value='".$ligne['design_id']."'";
		if ($idASelectionner==$ligne['design_id']) {
			$res.= " selected";
		}
		$res .= ">".$ligne['design_nom']."</option>";
	}
	
	$res .= "</select>";
	
	return $res;
}

function getSelectUpload($idASelectionner) {

	$res = '<select id="logoSite" name="logoSite"><option value="0">Par défaut</option>';

	connexion();
	$sql = sql("SELECT * FROM nuxt_upload ORDER BY upload_nom;");
	deconnexion();

	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<option value='".$ligne['upload_id']."'";
		if ($idASelectionner==$ligne['upload_id']) {
			$res.= " selected";
		}
		$res .= ">".$ligne['upload_nom']."</option>";
	}
	
	$res .= "</select>";
	
	return $res;
}

function xRemplirParamSite() {
	$reponse = new xajaxResponse();

	connexion();
	$sql = sql("SELECT * FROM nuxt_param INNER JOIN nuxt_design ON design_id = param_design_id_site LEFT JOIN nuxt_upload ON upload_id = param_upload_id ;");
	deconnexion();

	while($ligne = mysql_fetch_array($sql)) {
		$img = lireBdd($ligne['upload_chemin'],false);
		if($img=='') {
			$imgFinal = $ligne['design_logo_defaut'];
		}
		else {
			$imgFinal = $GLOBALS['uploadChemin'].$img;
		}
		$reponse->assign('nomSite','value',lireBdd($ligne['param_nom_site'],false));
		$reponse->assign('urlSite','value',lireBdd($ligne['param_url_site'],false));
		$reponse->assign('sloganSite','value',lireBdd($ligne['param_slogan_site'],false));
		$reponse->assign('listeLogoSite','innerHTML',getSelectUpload($ligne['param_upload_id']));
		$reponse->assign('logoImgSite','src',$imgFinal);
		$reponse->assign('desSite','value',lireBdd($ligne['param_description_site'],false));
		$reponse->assign('listeDesignSite','innerHTML',getSelectParam($ligne['design_id']));
	}
				
	return $reponse;
}

function xModifierParamSite($nom,$url,$slogan,$descr,$logo,$design) {
	$reponse = new xajaxResponse();
	
	connexion();
	$nom = securite_bdd($nom);
	$url = securite_bdd($url);
	$slogan = securite_bdd($slogan);
	$descr = securite_bdd($descr);
	$logo = securite_bdd($logo);
	$design = securite_bdd($design);
	
	$sql = sql("UPDATE nuxt_param SET param_nom_site = '".$nom."', param_url_site = '".$url."', param_slogan_site = '".$slogan."', param_description_site = '".$descr."', param_upload_id = '".$logo."', param_design_id_site = '".$design."' ;");
	deconnexion();

	$reponse->assign('lbl_resultat','innerHTML','Mises à jours des informations du site réussies');
	
	$reponse->call('xRemplirParamSite');
				
	return $reponse;
}
?>