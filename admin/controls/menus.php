<?php
function xAfficherMenus() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tableMenu','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Nom</th>
					<th>Module</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_menus INNER JOIN nuxt_modules ON modules_id = menus_modules_id WHERE menus_valide = 1 ORDER BY menus_nom;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['menus_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverMenu('.$value['menus_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverMenu('.$value['menus_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['menus_id'].'</td>';
			$tableau .= '<td>'.$value['menus_nom'].'</td>';
			$tableau .= '<td>'.$value['modules_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherMenuById('.$value['menus_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableMenu','innerHTML',$tableau);
        }
		
		return $reponse;
    }
    
function xAfficherMenuById($id) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_menus WHERE menus_id = '.$id.' ;');
		deconnexion();
			
		while($ligne = mysql_fetch_array($repSql)) {
			$reponse->assign('upid', 'value', $ligne['menus_id']);
			$reponse->assign('upnom', 'value', $ligne['menus_nom']);
			$reponse->assign('upListeModules', 'innerHTML', getSelectModules($id));
		}
			
		return $reponse;
}

function xCreerMenu($nom,$module) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableMenu', 'innerHTML');
		$reponse->clear('addListeModules', 'innerHTML');
		$reponse->assign('addListeModules', 'innerHTML', getSelectModules(""));
		
		$nextID = getNextID('menus');
			
		connexion();
			
		$nom = securite_bdd($nom);
		$module = securite_bdd($module);	
			
		sql('INSERT INTO nuxt_menus (menus_id,menus_nom,menus_modules_id,menus_valide) 
					VALUES ("'.$nextID.'", "'.$nom.'","'.$module.'","1");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Menu '.$nom.' crée avec succès !');
		$reponse->call('xajax_xAfficherMenus');
		
		return $reponse;
	}

function xModifierMenu($id,$nom,$module) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tableMenu','innerHTML');
		$reponse->clear('upListeModules','innerHTML');	
		connexion();
			
		$nom = securite_bdd($nom);
		$module = securite_bdd($module);	
			
		sql('UPDATE nuxt_menus SET menus_nom = "'.$nom.'", menus_modules_id = "'.$module.'" WHERE menus_id = "'.$id.'";');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Menu '.$nom.' modifié avec succès !');
		$reponse->call('xajax_xAfficherMenus');
		
		return $reponse;
	}

function xArchiverMenu($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tableMenu','innerHTML');
		connexion();
		
		sql('UPDATE nuxt_menus SET menus_valide = '.$value.' WHERE menus_id = '.$id.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Désactivation ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg.' du menu effectuée avec succès';
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->call('xajax_xAfficherMenus');
		
		return $reponse;
		
	}
	
function xAfficherMenusWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tableMenu','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " (menus_nom like '%".$crit."%') ";
    		$where .= " OR (modules_libelle like '%".$crit."%') ";
    	}

    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Nom</th>
					<th>Module</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_menus INNER JOIN nuxt_modules ON modules_id = menus_modules_id ".$where."  ORDER BY menus_nom;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['menus_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverMenu('.$value['menus_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverMenu('.$value['menus_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['menus_id'].'</td>';
			$tableau .= '<td>'.$value['menus_nom'].'</td>';
			$tableau .= '<td>'.$value['modules_libelle'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherMenuById('.$value['menus_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tableMenu','innerHTML',$tableau);
        }
		return $reponse;
    }

function getMenuModuleID($id) {
	connexion();
	$sql = sql("SELECT menus_modules_id FROM nuxt_menus WHERE menus_id = '".$id."';");
	deconnexion();
	
	$res = 0;
	
	while($ligne = mysql_fetch_array($sql)) {
		$res = $ligne['menus_modules_id'];
	}
	
	return $res;
}

function getSelectModules($id) {
	$res = "";

	if($id!="") {
		// je vais chercher l'id du profil du user sur lequel je bosse
		$idModule = getMenuModuleID($id);
		$res .= "<select name='upmodule' id='upmodule'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_modules LEFT JOIN nuxt_pages ON pages_modules_id = modules_id AND pages_valide = 1 WHERE modules_valide = 1 ORDER BY modules_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['modules_id']==$idModule) { $se = "selected";} else { $se = "";}
			$res .= "<option value='".$value['modules_id']."' ".$se." />".lireBdd($value['modules_libelle'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
	else {
		$res .= "<select name='addmodule' id='addmodule'>";
		connexion();
		$sql = sql("SELECT * FROM nuxt_modules WHERE modules_valide = 1 ORDER BY modules_libelle; ");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			$res .= "<option value='".$value['modules_id']."' />".lireBdd($value['modules_libelle'],false)."</option>";
	    }
	    $res .= "</select><br>";
	}
 
    return $res;
}
?>