<?php

class articles {

	// VARIABLES
	//Un utilisateur
	private $article;
	//Les utilisateurs
	private $articles;
	private $nb;
	
	private $id;
	private $titre;
	private $texte;
	private $date_creation;
	private $valide;
	private $createur_id;
	private $valideur_id;
	private $tags;
	private $categorie_id;


	// FONCTIONS VARIABLES
	public function getArticles() {
		$retour['nb'] = $this>nb;
       	$retour['tab'] = $this->articles;
		
		return $retour;
	}
	public function setArticles($value) {
		$this->articles[] = $value;
	}
	
	public function getNb() {
		return $this->nb;
	}
	
	public function getArticle() {
		$this->article;
	}
	public function setArticle($id,$titre,$texte,$date,$valide,$createur,$valideur,$tags,$cat) {		
        $this->setId($id);
        $this->setTitre($titre);
        $this->setTexte($texte);
        $this->setDateCreation($date);
        $this->setValide($valide);
        $this->setCreateur($createur);
        $this->setValideur($valideur);
        $this->setTags($tags);
        $this->setCategorie($cat);
        
        $article['id'] = $id;
        $article['titre'] = $titre;
        $article['texte'] = $texte;
        $article['date_creation'] = $date;
        $article['valide'] = $valide;
        $article['createur'] = $createur;
        $article['valideur'] = $valideur;
        $article['tags'] = $tags;
        $article['categorie_id'] = $cat;
    }
	
	public function getId() {
		return $this->id;
	}
	public function setId($value) {
		$this->id = $value;
	}
	
	public function getTitre() {
		return $this->titre;
	}
	public function setTitre($value) {
		$this->titre = $value;
	}
	
	public function getTexte() {
		return $this->texte;
	}
	public function setTexte($value) {
		$this->texte = $value;
	}
	
	public function getDateCreation() {
		return $this->date_creation;
	}
	public function setDateCreation($value) {
		$this->date_creation = $value;
	}
	
	public function getValide() {
		return $this->valide;
	}
	public function setValide($value) {
		$this->valide = $value;
	}
	
	public function getCreateur() {
		return $this->createur_id;
	}
	public function setCreateur($value) {
		$this->createur_id = $value;
	}
	
	public function getValideur() {
		return $this->valideur_id;
	}
	public function setValideur($value) {
		$this->valideur_id = $value;
	}
	
	public function getTags() {
		return $this->tags;
	}
	public function setTags($value) {
		$this->tags = $value;
	}
	
	public function getCategorie() {
		return $this->categorie_id;
	}
	public function setCategorie($value) {
		$this->categorie_id = $value;
	}
	
	// CONSTRUCTEUR
	
    
    public function __construct()
    {		
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tarticles'].' WHERE articles_valide = 1 ORDER BY articles_titre;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$article['id'] = $ligne['articles_id'];
	        $article['titre'] = $ligne['articles_titre'];
    	    $article['texte'] = $ligne['articles_texte'];
        	$article['date_creation'] = $ligne['articles_date_creation'];
	        $article['createur_id'] = $ligne['articles_users_id_createur'];
    	    $article['valideur_id'] = $ligne['articles_users_id_valideur'];
        	$article['valide'] = $ligne['articles_valide'];
	        $article['tags'] = $ligne['articles_tags'];
    	    $article['categorie_id'] = $ligne['articles_categories_id'];
        	
        	$this->setArticles($article);
        }	
		
		$this->nb = mysql_num_rows($reponse);
    }
	
	// FONCTIONS BDD
	public function _getById($id)
    {		
	    // Je vide le tableau de articles
    	$this->articles = "";
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tarticles'].' WHERE articles_id = '.$id.';');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$article['id'] = $ligne['articles_id'];
	        $article['titre'] = $ligne['articles_titre'];
    	    $article['texte'] = $ligne['articles_texte'];
        	$article['date_creation'] = $ligne['articles_date_creation'];
	        $article['createur_id'] = $ligne['articles_users_id_createur'];
    	    $article['valideur_id'] = $ligne['articles_users_id_valideur'];
        	$article['valide'] = $ligne['articles_valide'];
	        $article['tags'] = $ligne['articles_tags'];
    	    $article['categorie_id'] = $ligne['articles_categories_id'];
        }	
		
		return $article;
    }
    
    public function _getNbByValide($val)
    {		
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT articles_id FROM '.$GLOBALS['Tarticles'].' WHERE articles_valide = '.$val.';');
		deconnexion();
		
		return mysql_num_rows($reponse);
    }
    
    public function _getWCritere($crit)
    {		
    	// Je vide le tableau de articles
    	$this->articles = "";
    	$this->nb = 0;
    	
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	$jointure = " INNER JOIN ".$GLOBALS['Tusers']." ON users_id = art.articles_users_id_createur ";
    	$jointure .= " INNER JOIN ".$GLOBALS['Tcategories']." cat ON cat.articles_categories_id = art.articles_categories_id ";
    	
    	// Découpage des critères
    	if(($crit['recherche']!="") AND (isset($crit['recherche']))) {
    		$where .= " art.articles_titre like '%".$crit['recherche']."%' ";
    		$where .= " OR art.articles_texte like '%".$crit['recherche']."%' ";
    		$where .= " OR users_login like '%".$crit['recherche']."%' ";
    		$where .= " OR cat.articles_categories_libelle like '%".$crit['recherche']."%' ";
    		$where .= " OR art.articles_tags like '%".$crit['recherche']."%' ";
    	}
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql("SELECT * FROM ".$GLOBALS['Tarticles']." art ".$jointure." ".$where." ;");
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$article['id'] = $ligne['articles_id'];
	        $article['titre'] = $ligne['articles_titre'];
    	    $article['texte'] = $ligne['articles_texte'];
        	$article['date_creation'] = $ligne['articles_date_creation'];
	        $article['createur_id'] = $ligne['articles_users_id_createur'];
    	    $article['valideur_id'] = $ligne['articles_users_id_valideur'];
        	$article['valide'] = $ligne['articles_valide'];
	        $article['tags'] = $ligne['articles_tags'];
    	    $article['categorie_id'] = $ligne['articles_categories_id'];
        	
        	$this->setArticles($article);
        }	
        
        $this->nb = mysql_num_rows($reponse);
       	$retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $this->articles;
		
		return $retour;
    }
    
	public function _add($a) {
		try {
			connexion();
			
			$titre = securite_bdd($a['titre']);
			$texte = securite_bdd($a['texte']);
			$date_creation = date();
			$createur = securite_bdd($a['createur_id']);
			$valideur = securite_bdd($a['valideur_id']);
			$valide = securite_bdd($a['valide']);
			$tags = securite_bdd($a['tags']);
			$cat = securite_bdd($a['categorie_id']);
			
			
			sql('INSERT INTO '.$GLOBALS['Tarticles'].' (articles_titre, articles_texte, articles_date_creation, articles_users_id_createur, articles_users_id_valideur, articles_valide, articles_tags, articles_categories_id) 
					VALUES ("'.$titre.'", "'.$texte.'", "'.$date_creation.'", "'.$createur.'", "'.$valideur.'", "'.$valide.'", "'.$tags.'", "'.$cat.'");');

			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _del($id,$value) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tarticles'].' SET articles_valide = '.$value.' WHERE articles_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _up($a) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tarticles'].' SET articles_valide = 0 WHERE users_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
}


?>