<?php

class users {

	// VARIABLES
	//Un utilisateur
	private $user;
	//Les utilisateurs
	private $users;
	private $nb;
	
	private $id;
	private $login;
	private $password;
	private $mail;
	private $portable;
	private $fixe;
	private $avatar;
	private $date_creation;
	private $facebook;
	private $twitter;	
	private $site;
	private $valide;
	private $profil_id;

	// FONCTIONS VARIABLES
	public function getUsers() {
		$retour['nb'] = $this->nb;
       	$retour['tab'] = $this->users;
		
		return $retour;
	}
	public function setUsers($value) {
		$this->users[] = $value;
	}
	
	public function getNb() {
		return $this->nb;
	}
	
	public function getUser() {
		$this->user;
	}
	public function setUser($id,$login,$pwd,$mail,$port,$fix,$av,$date,$fa,$tw,$site,$valide,$profil) {		
        $this->setId($id);
        $this->setLogin($login);
        $this->setPassword($pwd);
        $this->setMail($login);
        $this->setPortable($port);
        $this->setFixe($fix);
        $this->setAvatar($av);
        $this->setDateCreation($date);
        $this->setFacebook($fa);
        $this->setTwitter($tw);
        $this->setSite($site);
        $this->setValide($valide);
        $this->setProfilId($profil);
        
        $user['id'] = $id;
        $user['login'] = $login;
        $user['password'] = $pwd;
        $user['mail'] = $mail;
        $user['portable'] = $port;
        $user['fixe'] = $fix;
        $user['avatar'] = $av;
        $user['date_creation'] = $date;
        $user['facebook'] = $fa;
        $user['twitter'] = $tw;
        $user['site'] = $site;
        $user['valide'] = $valide;
        $user['profil_id'] = $profil;
    }
	
	public function getId() {
		return $this->id;
	}
	public function setId($value) {
		$this->id = $value;
	}
	
	public function getLogin() {
		return $this->login;
	}
	public function setLogin($value) {
		$this->login = $value;
	}
	
	public function getPassword() {
		return $this->password;
	}
	public function setPassword($value) {
		$this->password = $value;
	}
	
	public function getMail() {
		return $this->mail;
	}
	public function setMail($value) {
		$this->mail = $value;
	}
	
	public function getPortable() {
		return $this->portable;
	}
	public function setPortable($value) {
		$this->portable = $value;
	}
	
	public function getFixe() {
		return $this->fixe;
	}
	public function setFixe($value) {
		$this->fixe = $value;
	}
	
	public function getAvatar() {
		return $this->avatar;
	}
	public function setAvatar($value) {
		$this->avatar = $value;
	}
	
	public function getDateCreation() {
		return $this->date_creation;
	}
	public function setDateCreation($value) {
		$this->date_creation = $value;
	}
	
	public function getFacebook() {
		return $this->facebook;
	}
	public function setFacebook($value) {
		$this->facebook = $value;
	}
	
	public function getTwitter() {
		return $this->twitter;
	}
	public function setTwitter($value) {
		$this->twitter = $value;
	}
	
	public function getSite() {
		return $this->site;
	}
	public function setSite($value) {
		$this->site = $value;
	}
	
	public function getValide() {
		return $this->valide;
	}
	public function setValide($value) {
		$this->$valide = $value;
	}
	
	public function getProfilId() {
		return $this->profil_id;
	}
	public function setProfilId($value) {
		$this->profil_id = $value;
	}

	// CONSTRUCTEUR
	
    
    public function __construct()
    {		
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tusers'].' WHERE users_valide = 1 AND users_id > 0 ORDER BY users_login;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$user['id'] = $ligne['users_id'];
	        $user['login'] = $ligne['users_login'];
    	    $user['password'] = $ligne['users_password'];
        	$user['mail'] = $ligne['users_mail'];
	        $user['portable'] = $ligne['users_portable'];
    	    $user['fixe'] = $ligne['users_fixe'];
        	$user['avatar'] = $ligne['users_avatar'];
	        $user['date_creation'] = $ligne['users_date_creation'];
    	    $user['facebook'] = $ligne['users_facebook'];
        	$user['twitter'] = $ligne['users_twitter'];
	        $user['site'] = $ligne['users_site_web'];
    	    $user['valide'] = $ligne['users_valide'];
        	$user['profil_id'] = $ligne['users_profil_id'];
        	
        	$this->setUsers($user);
        }	
		
		$this->nb = mysql_num_rows($reponse);
    }
	
	// FONCTIONS BDD
	public function _getById($id)
    {		
    	// Je vide le tableau de users
    	$this->users = "";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tusers'].' WHERE users_id = '.$id.' AND users_id > 0 ;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$user['id'] = $ligne['users_id'];
	        $user['login'] = $ligne['users_login'];
    	    $user['password'] = $ligne['users_password'];
        	$user['mail'] = $ligne['users_mail'];
	        $user['portable'] = $ligne['users_portable'];
    	    $user['fixe'] = $ligne['users_fixe'];
        	$user['avatar'] = $ligne['users_avatar'];
	        $user['date_creation'] = $ligne['users_date_creation'];
    	    $user['facebook'] = $ligne['users_facebook'];
        	$user['twitter'] = $ligne['users_twitter'];
	        $user['site'] = $ligne['users_site_web'];
    	    $user['valide'] = $ligne['users_valide'];
        	$user['profil_id'] = $ligne['users_profil_id'];
        }	
        
        $this->nb = mysql_num_rows($reponse);
        $retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $user;
		
		return $retour;
    }
    
    public function _getNbByValide($val)
    {		
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT users_id FROM '.$GLOBALS['Tusers'].' WHERE users_valide = '.$val.' AND users_id > 0 ;');
		deconnexion();
		
		return mysql_num_rows($reponse);
    }
    
    public function _getWCritere($crit)
    {		
    	// Je vide le tableau de users
    	$this->users = "";
    	
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit['recherche']!="") OR (isset($crit['recherche']))) {
    		$where .= " (users_login like '%".$crit['recherche']."%' ";
    		$where .= " OR users_mail like '%".$crit['recherche']."%' ";
    		$where .= " OR users_fixe like '%".$crit['recherche']."%' ";
    		$where .= " OR users_portable like '%".$crit['recherche']."%' ";
    		$where .= " OR users_facebook like '%".$crit['recherche']."%' ";
    		$where .= " OR users_twitter like '%".$crit['recherche']."%')  AND users_id > 0 ";
    	}

    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql("SELECT * FROM ".$GLOBALS['Tusers']." ".$where." ;");
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$user['id'] = $ligne['users_id'];
	        $user['login'] = $ligne['users_login'];
    	    $user['password'] = $ligne['users_password'];
        	$user['mail'] = $ligne['users_mail'];
	        $user['portable'] = $ligne['users_portable'];
    	    $user['fixe'] = $ligne['users_fixe'];
        	$user['avatar'] = $ligne['users_avatar'];
	        $user['date_creation'] = $ligne['users_date_creation'];
    	    $user['facebook'] = $ligne['users_facebook'];
        	$user['twitter'] = $ligne['users_twitter'];
	        $user['site'] = $ligne['users_site_web'];
    	    $user['valide'] = $ligne['users_valide'];
        	$user['profil_id'] = $ligne['users_profil_id'];
        	
        	$this->setUsers($user);
        }	
        
        $this->nb = mysql_num_rows($reponse);
       	$retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $this->users;
		
		return $retour;
    }
    
	public function _add($login,$password,$mail) {
		try {
			connexion();
			
			$login = securite_bdd($login);
			$password = securite_bdd($password);
			$mail = securite_bdd($mail);
			
			sql('INSERT INTO '.$GLOBALS['Tusers'].' (users_login, users_password, users_mail) 
					VALUES ("'.$login.'", "'.$password.'", "'.$mail.'");');
			
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _del($id,$value) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tusers'].' SET users_valide = '.$value.' WHERE users_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _up($u) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tusers'].' SET users_valide = 0 WHERE users_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	
}


?>