<?php

class pages {

	// VARIABLES
	//Un page
	private $page;
	//Les pages
	private $pages;
	private $nb;
	
	private $id;
	private $titre;
	private $texte;
	private $url;
	private $valide;

	// FONCTIONS VARIABLES
	public function getPages() {
		$retour['nb'] = $this>nb;
       	$retour['tab'] = $this->pages;
		
		return $retour;
	}
	public function setPages($value) {
		$this->pages[] = $value;
	}
	
	public function getNb() {
		return $this->nb;
	}
	
	public function getPage() {
		$this->page;
	}
	public function setPage($id,$titre,$texte,$url,$valide) {		
        $this->setId($id);
        $this->setTitre($titre);
        $this->setTexte($texte);
        $this->setUrl($url);
        $this->setValide($valide);
        
        $page['id'] = $id;
        $page['titre'] = $titre;
        $page['texte'] = $texte;
        $page['url'] = $url;
        $page['valide'] = $valide;
    }
	
	public function getId() {
		return $this->id;
	}
	public function setId($value) {
		$this->id = $value;
	}
	
	public function getTitre() {
		return $this->titre;
	}
	public function setTitre($value) {
		$this->titre = $value;
	}
	
	public function getTexte() {
		return $this->texte;
	}
	public function setTexte($value) {
		$this->texte = $value;
	}
	
	public function getUrl() {
		return $this->url;
	}
	public function setUrl($value) {
		$this->url = $value;
	}
	
	public function getValide() {
		return $this->valide;
	}
	public function setValide($value) {
		$this->$valide = $value;
	}

	// CONSTRUCTEUR
	
    
    public function __construct()
    {		
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tpages'].' WHERE pages_valide = 1 ORDER BY pages_titre;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$page['id'] = $ligne['pages_id'];
	        $page['titre'] = $ligne['pages_titre'];
	        $page['texte'] = $ligne['pages_texte'];
	        $page['page'] = $ligne['pages_url'];
    	    $page['valide'] = $ligne['pages_valide'];
        	
        	$this->setPages($page);
        }	
		
		$this->nb = mysql_num_rows($reponse);
    }
	
	// FONCTIONS BDD
	public function _getById($id)
    {		
    	// Je vide le tableau de categories
    	$this->pages = "";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tpages'].' WHERE pages_id = '.$id.';');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$page['id'] = $ligne['pages_id'];
	        $page['titre'] = $ligne['pages_titre'];
	        $page['texte'] = $ligne['pages_texte'];
	        $page['url'] = $ligne['pages_url'];
    	    $page['valide'] = $ligne['pages_valide'];
        }	
        
        $this->nb = mysql_num_rows($reponse);
        $retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $page;
		
		return $retour;
    }
    
    public function _getNbByValide($val)
    {		
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT pages_id FROM '.$GLOBALS['Tpages'].' WHERE pages_valide = '.$val.';');
		deconnexion();
		
		return mysql_num_rows($reponse);
    }
    
    public function _getWCritere($crit)
    {		
    	// Je vide le tableau de users
    	$this->pages = "";
    	
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit['recherche']!="") OR (isset($crit['recherche']))) {
    		$where .= " pages_titre like '%".$crit['recherche']."%' OR pages_texte like '%".$crit['recherche']."%' ;";
		}

    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql("SELECT * FROM ".$GLOBALS['Tpages']." ".$where." ;");
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$page['id'] = $ligne['pages_id'];
	        $page['titre'] = $ligne['pages_titre'];
	        $page['texte'] = $ligne['pages_texte'];
	        $page['url'] = $ligne['pages_url'];
    	    $page['valide'] = $ligne['pages_valide'];
        	
        	$this->setPages($page);
        }	
        
        $this->nb = mysql_num_rows($reponse);
       	$retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $this->pages;
		
		return $retour;
    }
    
	public function _add($titre,$texte,$url) {
		try {
			connexion();
			
			$titre = securite_bdd($titre);
			
			sql('INSERT INTO '.$GLOBALS['Tpages'].' (pages_titre, pages_texte, pages_url, pages_valide) 
					VALUES ("'.$titre.'", "'.$texte.'", "'.$url.'", "1");');
					
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _del($id,$value) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tpages'].' SET pages_valide = '.$value.' WHERE pages_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _up($c) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tpages'].' SET pages_valide = 0 WHERE pages_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
}


?>