<?php

class menus {

	// VARIABLES
	//Un menu
	private $menu;
	//Les menus
	private $menus;
	private $nb;
	
	private $id;
	private $nom;
	private $url;
	private $valide;

	// FONCTIONS VARIABLES
	public function getMenus() {
		$retour['nb'] = $this>nb;
       	$retour['tab'] = $this->menus;
		
		return $retour;
	}
	public function setMenus($value) {
		$this->menus[] = $value;
	}
	
	public function getNb() {
		return $this->nb;
	}
	
	public function getMenu() {
		$this->menu;
	}
	public function setMenu($id,$nom,$url,$valide) {		
        $this->setId($id);
        $this->setNom($nom);
        $this->setUrl($url);
        $this->setValide($valide);
        
        $menu['id'] = $id;
        $menu['nom'] = $nom;
        $menu['url'] = $url;
        $menu['valide'] = $valide;
    }
	
	public function getId() {
		return $this->id;
	}
	public function setId($value) {
		$this->id = $value;
	}
	
	public function getNom() {
		return $this->nom;
	}
	public function setNom($value) {
		$this->nom = $value;
	}
	
	public function getUrl() {
		return $this->url;
	}
	public function setUrl($value) {
		$this->url = $value;
	}
	
	public function getValide() {
		return $this->valide;
	}
	public function setValide($value) {
		$this->$valide = $value;
	}

	// CONSTRUCTEUR
	
    
    public function __construct()
    {		
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tmenus'].' WHERE menus_valide = 1 ORDER BY menus_nom;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$menu['id'] = $ligne['menus_id'];
	        $menu['nom'] = $ligne['menus_nom'];
	        $menu['url'] = $ligne['menus_url'];
    	    $menu['valide'] = $ligne['menus_valide'];
        	
        	$this->setMenus($menu);
        }	
		
		$this->nb = mysql_num_rows($reponse);
    }
	
	// FONCTIONS BDD
	public function _getById($id)
    {		
    	// Je vide le tableau de categories
    	$this->menus = "";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tmenus'].' WHERE menus_id = '.$id.';');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$menu['id'] = $ligne['menus_id'];
	        $menu['nom'] = $ligne['menus_nom'];
	        $menu['url'] = $ligne['menus_url'];
    	    $menu['valide'] = $ligne['menus_valide'];
        }	
        
        $this->nb = mysql_num_rows($reponse);
        $retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $menu;
		
		return $retour;
    }
    
    public function _getNbByValide($val)
    {		
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT menus_id FROM '.$GLOBALS['Tmenus'].' WHERE menus_valide = '.$val.';');
		deconnexion();
		
		return mysql_num_rows($reponse);
    }
    
    public function _getWCritere($crit)
    {		
    	// Je vide le tableau de users
    	$this->menus = "";
    	
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit['recherche']!="") OR (isset($crit['recherche']))) {
    		$where .= " menus_nom like '%".$crit['recherche']."%';";
		}

    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql("SELECT * FROM ".$GLOBALS['Tmenus']." ".$where." ;");
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$menu['id'] = $ligne['menus_id'];
	        $menu['nom'] = $ligne['menus_nom'];
	        $menu['url'] = $ligne['menus_url'];
    	    $menu['valide'] = $ligne['menus_valide'];
        	
        	$this->setMenus($menu);
        }	
        
        $this->nb = mysql_num_rows($reponse);
       	$retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $this->menus;
		
		return $retour;
    }
    
	public function _add($nom,$url) {
		try {
			connexion();
			
			$nom = securite_bdd($nom);
			
			sql('INSERT INTO '.$GLOBALS['Tmenus'].' (menus_nom, menus_url, menus_valide) 
					VALUES ("'.$nom.'", "'.$url.'", "1");');
					
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _del($id,$value) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tmenus'].' SET menus_valide = '.$value.' WHERE menus_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _up($c) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tmenus'].' SET menus_valide = 0 WHERE menus_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
}


?>