<?php

class categories {

	// VARIABLES
	//Une catégorie
	private $categorie;
	//Les catégories
	private $categories;
	private $nb;
	
	private $id;
	private $libelle;
	private $valide;

	// FONCTIONS VARIABLES
	public function getCategories() {
		$retour['nb'] = $this>nb;
       	$retour['tab'] = $this->categories;
		
		return $retour;
	}
	public function setCategories($value) {
		$this->categories[] = $value;
	}
	
	public function getNb() {
		return $this->nb;
	}
	
	public function getCategorie() {
		$this->categorie;
	}
	public function setCategorie($id,$libelle,$valide) {		
        $this->setId($id);
        $this->setLibelle($libelle);
        $this->setValide($valide);
        
        $categorie['id'] = $id;
        $categorie['libelle'] = $libelle;
        $categorie['valide'] = $valide;
    }
	
	public function getId() {
		return $this->id;
	}
	public function setId($value) {
		$this->id = $value;
	}
	
	public function getLibelle() {
		return $this->libelle;
	}
	public function setLibelle($value) {
		$this->libelle = $value;
	}
	
	public function getValide() {
		return $this->valide;
	}
	public function setValide($value) {
		$this->$valide = $value;
	}

	// CONSTRUCTEUR
	
    
    public function __construct()
    {		
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tcategories'].' WHERE articles_categories_valide = 1 ORDER BY articles_categories_libelle;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$categorie['id'] = $ligne['articles_categories_id'];
	        $categorie['libelle'] = $ligne['articles_categories_libelle'];
    	    $categorie['valide'] = $ligne['articles_categories_valide'];
        	
        	$this->setCategories($categorie);
        }	
		
		$this->nb = mysql_num_rows($reponse);
    }
	
	// FONCTIONS BDD
	public function _getById($id)
    {		
    	// Je vide le tableau de categories
    	$this->categories = "";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tcategories'].' WHERE articles_categories_id = '.$id.';');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$categorie['id'] = $ligne['articles_categories_id'];
	        $categorie['libelle'] = $ligne['articles_categories_libelle'];
    	    $categorie['valide'] = $ligne['articles_categories_valide'];
        }	
        
        $this->nb = mysql_num_rows($reponse);
        $retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $categorie;
		
		return $retour;
    }
    
    public function _getNbByValide($val)
    {		
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT articles_categories_id FROM '.$GLOBALS['Tcategories'].' WHERE articles_categories_valide = '.$val.';');
		deconnexion();
		
		return mysql_num_rows($reponse);
    }
    
    public function _getWCritere($crit)
    {		
    	// Je vide le tableau de users
    	$this->categories = "";
    	
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit['recherche']!="") OR (isset($crit['recherche']))) {
    		$where .= " articles_categories_libelle like '%".$crit['recherche']."%' ";
		}

    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql("SELECT * FROM ".$GLOBALS['Tcategories']." ".$where." ;");
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$categorie['id'] = $ligne['articles_categories_id'];
	        $categorie['libelle'] = $ligne['articles_categories_libelle'];
    	    $categorie['valide'] = $ligne['articles_categories_valide'];
        	
        	$this->setCategories($categorie);
        }	
        
        $this->nb = mysql_num_rows($reponse);
       	$retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $this->categories;
		
		return $retour;
    }
    
	public function _add($libelle) {
		try {
			connexion();
			
			$libelle = securite_bdd($libelle);
			
			sql('INSERT INTO '.$GLOBALS['Tcategories'].' (articles_categories_libelle, articles_categories_valide) 
					VALUES ("'.$libelle.'", "1");');
					
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _del($id,$value) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tcategories'].' SET articles_categories_valide = '.$value.' WHERE articles_categories_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _up($c) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tcategories'].' SET articles_categories_valide = 0 WHERE articles_categories_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
}


?>