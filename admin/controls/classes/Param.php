<?php

class param
{
	// Chaque variable correspond à une table en base de données
	//private $Tusers;
	//private $Tarticles;

	// Constructeur pour initier la valeur des tables et autres
	public function __construct($prefixe)
    {
    
        //$this->Tusers = 'nuxt_users';
        //$this->Tarticles = 'nuxt_articles';
        $GLOBALS['Tusers'] = $prefixe.'users';
        $GLOBALS['Tprofils'] = $prefixe.'profils';
        $GLOBALS['Tarticles'] = $prefixe.'articles';
        $GLOBALS['Tcategories'] = $prefixe.'articles_categories';
        $GLOBALS['Tpages'] = $prefixe.'pages';
        $GLOBALS['Tmenus'] = $prefixe.'menus';
    }

    static public function set($name, $value)
    {
        $GLOBALS[$name] = $value;
    }

    static public function get($name)
    {
        return $GLOBALS[$name];
    }
    
}

?>