<?php

class profils {

	// VARIABLES
	//Un profil
	private $profil;
	//Les catégories
	private $profils;
	private $nb;
	
	private $id;
	private $libelle;
	private $valide;

	// FONCTIONS VARIABLES
	public function getProfils() {
		$retour['nb'] = $this>nb;
       	$retour['tab'] = $this->profils;
		
		return $retour;
	}
	public function setProfils($value) {
		$this->profils[] = $value;
	}
	
	public function getNb() {
		return $this->nb;
	}
	
	public function getProfil() {
		$this->profil;
	}
	public function setProfil($id,$libelle,$valide) {		
        $this->setId($id);
        $this->setLibelle($libelle);
        $this->setValide($valide);
        
        $profil['id'] = $id;
        $profil['libelle'] = $libelle;
        $profil['valide'] = $valide;
    }
	
	public function getId() {
		return $this->id;
	}
	public function setId($value) {
		$this->id = $value;
	}
	
	public function getLibelle() {
		return $this->libelle;
	}
	public function setLibelle($value) {
		$this->libelle = $value;
	}
	
	public function getValide() {
		return $this->valide;
	}
	public function setValide($value) {
		$this->$valide = $value;
	}

	// CONSTRUCTEUR
	
    
    public function __construct()
    {		
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tprofils'].' WHERE profils_valide = 1 AND profils_id > 0 ORDER BY profils_libelle;');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$profil['id'] = $ligne['profils_id'];
	        $profil['libelle'] = $ligne['profils_libelle'];
    	    $profil['valide'] = $ligne['profils_valide'];
        	
        	$this->setProfils($profil);
        }	
		
		$this->nb = mysql_num_rows($reponse);
    }
	
	// FONCTIONS BDD
	public function _getById($id)
    {		
    	// Je vide le tableau de categories
    	$this->profils = "";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT * FROM '.$GLOBALS['Tprofils'].' WHERE profils_id = '.$id.';');
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$profil['id'] = $ligne['profils_id'];
	        $profil['libelle'] = $ligne['profils_libelle'];
    	    $profil['valide'] = $ligne['profils_valide'];
        }	
        
        $this->nb = mysql_num_rows($reponse);
        $retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $profil;
		
		return $retour;
    }
    
    public function _getNbByValide($val)
    {		
    
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql('SELECT profils_id FROM '.$GLOBALS['Tprofils'].' WHERE profils_valide = '.$val.' AND profils_id > 0;');
		deconnexion();
		
		return mysql_num_rows($reponse);
    }
    
    public function _getWCritere($crit)
    {		
    	// Je vide le tableau de users
    	$this->profils = "";
    	
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit['recherche']!="") OR (isset($crit['recherche']))) {
    		$where .= " profils_libelle like '%".$crit['recherche']."%' AND profils_id > 0 ";
		}

    	
        // Récupérer en base de données les infos du membre
        connexion();
		$reponse = sql("SELECT * FROM ".$GLOBALS['Tprofils']." ".$where." ;");
		deconnexion();
		
		while($ligne = mysql_fetch_array($reponse)) {
			$profil['id'] = $ligne['profils_id'];
	        $profil['libelle'] = $ligne['profils_libelle'];
    	    $profil['valide'] = $ligne['profils_valide'];
        	
        	$this->setProfils($profil);
        }	
        
        $this->nb = mysql_num_rows($reponse);
       	$retour['nb'] = mysql_num_rows($reponse);
       	$retour['tab'] = $this->profils;
		
		return $retour;
    }
    
	public function _add($libelle) {
		try {
			connexion();
			
			$libelle = securite_bdd($libelle);
			
			sql('INSERT INTO '.$GLOBALS['Tprofils'].' (profils_libelle, profils_valide) 
					VALUES ("'.$libelle.'", "1");');
					
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _del($id,$value) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tprofils'].' SET profils_valide = '.$value.' WHERE profils_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
	
	public function _up($c) {
		try {
			connexion();
			sql('UPDATE '.$GLOBALS['Tprofils'].' SET profils_valide = 0 WHERE profils_id = '.$id.';');
			deconnexion();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
	}
}


?>