<?php
function xAfficherPages() {

    	$reponse = new xajaxResponse();
    	$reponse->clear('tablePage','innerHTML');
    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Titre</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
    	
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql('SELECT * FROM nuxt_pages WHERE pages_valide = 1 ORDER BY pages_titre;');
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['pages_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverPage('.$value['pages_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverPage('.$value['pages_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['pages_id'].'</td>';
			$tableau .= '<td>'.$value['pages_titre'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherPageById('.$value['pages_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }	
        if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tablePage','innerHTML',$tableau);
        }
		
		return $reponse;
    }
    
function xAfficherPageById($id) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_pages WHERE pages_id = '.$id.' ;');
		deconnexion();
			
		while($ligne = mysql_fetch_array($repSql)) {
			$reponse->assign('upid', 'value', $ligne['pages_id']);
			$reponse->assign('upmid', 'value', $ligne['pages_modules_id']);
			$reponse->assign('uptitre', 'value', $ligne['pages_titre']);
			$reponse->assign('uptexte', 'innerHTML', $ligne['pages_texte']);
		}
			
		return $reponse;
}

function xCreerPage($titre,$texte) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tablePage','innerHTML');

		$nextID_page = 1;
		$nextID_module = 1;
		
		$nextID_page = getNextID('pages');
		$nextID_module = getNextID('modules');
		
		connexion();
			
		$titre = securite_bdd($titre);
		$texte = securite_bdd($texte);
		$url = 'pages.php?id='.$nextID_page;
			
		//Je crée un module lié à cette page	
		sql('INSERT INTO nuxt_modules (modules_id,modules_libelle,modules_pages,modules_valide)
				VALUES ("'.$nextID_module.'","'.$titre.'","'.$url.'","1");');	

		//le futur nom du fichier est dans la variable $url
		sql('INSERT INTO nuxt_pages (pages_id,pages_titre,pages_texte,pages_url,pages_valide,pages_modules_id) 
				VALUES ("'.$nextID_page.'", "'.$titre.'","'.$texte.'","'.$url.'","1","'.$nextID_module.'");');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Page '.$titre.' créée avec succès !');
		$reponse->call('xajax_xAfficherPages');
		
		return $reponse;
	}

function xModifierPage($id,$mId,$titre,$texte) {
		
		$reponse = new xajaxResponse();
		$reponse->clear('tablePage','innerHTML');	
		connexion();
			
		$titre = securite_bdd($titre);
		$texte = securite_bdd($texte);	
			
		sql('UPDATE nuxt_pages SET pages_titre = "'.$titre.'", pages_texte = "'.$texte.'" WHERE pages_id = "'.$id.'";');
		sql('UPDATE nuxt_modules SET modules_libelle = "'.$titre.'" WHERE modules_id = "'.$mId.'";');
			
		deconnexion();
		
		$reponse->assign('lbl_resultat', 'innerHTML', 'Page '.$titre.' modifiée avec succès !');
		$reponse->call('xajax_xAfficherPages');
		
		return $reponse;
	}

function xArchiverPage($id,$value) {

		$reponse = new xajaxResponse();
		$reponse->clear('tablePage','innerHTML');
		connexion();
		
		sql('UPDATE nuxt_pages SET pages_valide = '.$value.' WHERE pages_id = '.$id.';');
		
		// J'archive aussi le menu du module associé
		$idModule = getModulePageId($id);
		sql('UPDATE nuxt_menus SET menus_valide = '.$value.' WHERE menus_modules_id = '.$idModule.';');
		
		deconnexion();
		
		if($value=="0") {
			$msg = "Désactivation ";
		}
		else {
			$msg = "Activation ";
		}
		$res = $msg.' de la page effectuée avec succès';
		
		$reponse->assign('lbl_resultat','innerHTML',$res);
		$reponse->call('xajax_xAfficherPages');
		
		return $reponse;
		
	}
	
function xAfficherPagesWCritere($crit)
    {		
		$reponse = new xajaxResponse();
    	$reponse->clear('tablePage','innerHTML');
    	// Variable pour le WHERE
    	$where = " WHERE ";
    	
    	// Découpage des critères
    	if(($crit!="") OR (isset($crit))) {
    		$where .= " (pages_titre like '%".$crit."%') ";
    		$where .= " OR (pages_texte like '%".$crit."%') ";
    	}

    	$tableau = "";
    	$tableau .= "<tr>
					<th>ID</th>
					<th>Titre</th>
					<th>Modifier</th>
					<th>Archiver</th>
				</tr>";
        // Récupérer en base de données les infos du membre
        connexion();
		$sql = sql("SELECT * FROM nuxt_pages ".$where."  ORDER BY pages_titre;");
		deconnexion();
		
		while($value = mysql_fetch_array($sql)) {
			if($value['pages_valide']=='1') {
				$image_del = '<img src="'.$GLOBALS['IMG_valide'].'" alt="Actif" class="icone" onClick="jsArchiverPage('.$value['pages_id'].',0);" />';
			}
			else {
				$image_del = '<img src="'.$GLOBALS['IMG_invalide'].'" alt="Inactif" class="icone"  onClick="jsArchiverPage('.$value['pages_id'].',1);" />';			
			}
			$image_up = '<img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" />';
			
			$tableau .= '<tr>';
			$tableau .= '<td>'.$value['pages_id'].'</td>';
			$tableau .= '<td>'.$value['pages_titre'].'</td>';
			$tableau .= '<td><img src="'.$GLOBALS['IMG_modifier'].'" alt="Modifier" class="icone" onClick="jsAfficherPageById('.$value['pages_id'].');" /></td>';
			$tableau .= '<td>'.$image_del.'</td>';
			$tableau .= '</tr>';
        }
		
		if(mysql_num_rows($sql)<=0) {
        	$reponse->assign('lbl_resultat','innerHTML','Aucun résultat');
        }
        else {
			$reponse->assign('tablePage','innerHTML',$tableau);
        }
		return $reponse;
    }
    
function getModulePageId($id) {
	$res = 0;
	
	connexion();
	$sql = sql('SELECT pages_modules_id FROM nuxt_pages WHERE pages_id = '.$id);
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {
		$res = $ligne['pages_modules_id'];
	}
	
	return $res;
}
?>