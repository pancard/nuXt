<?php
	include('config.php');

	// Include des classes
	include('./controls/classes/Param.php');
	// Paramètre global
	// J'instancie ma classe Param avec la valeur du préfixe des tables.
	// Ce qui valorisera les variables globals du nom des tables.
	$p = new param($GLOBALS['prefixe']);
	

	include('./controls/classes/Users.php');
	include('./controls/classes/Profils.php');
	include('./controls/classes/Categories.php');
	include('./controls/classes/Articles.php');
	include('./controls/classes/Pages.php');
	include('./controls/classes/Menus.php');
	
	// Include de la bibliothèque
	include('./bibliotheque/connexion.php');
	include('./bibliotheque/fonctions.php');
	
	include('./controls/users.php');
	include('./controls/profils.php');
	include('./controls/categories.php');
	include('./controls/articles.php');
	include('./controls/pages.php');
	include('./controls/menus.php');
	include('./controls/param.php');
	include('./controls/upload.php');

	$userLogin = $_SESSION['users_login'];
	$userId = $_SESSION['users_id'];
	$userAvatar = $_SESSION['users_avatar'];
	// listes des menus / modules accessible pour le user
	if($userId!='') {
		$droit = getDroitsUsers($userId);
	}
?>