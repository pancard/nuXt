<?php
	// On démarre la session AVANT d'écrire du code HTML
	session_start();
	include('global.php');
	// On s'occupe de tout ce qui est xAjax
	require_once('./bibliotheque/xajax/xajax_core/xajax.inc.php');
	$xajax = new xajax(); //On initialise l'objet xajax.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherUserById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerUser');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierUser');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverUser');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherUsers');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherUsersWCritere');// On enregistre nos fonctions.
	
	$xajax->register(XAJAX_FUNCTION, 'xAfficherProfilById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerProfil');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierProfil');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverProfil');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherProfils');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherProfilsWCritere');// On enregistre nos fonctions.
	
	$xajax->register(XAJAX_FUNCTION, 'xAfficherCategorieById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerCategorie');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierCategorie');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverCategorie');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherCategories');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherCategoriesWCritere');// On enregistre nos fonctions.
	
	$xajax->register(XAJAX_FUNCTION, 'xAfficherMenuById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerMenu');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierMenu');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverMenu');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherMenus');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherMenusWCritere');// On enregistre nos fonctions.
	
	$xajax->register(XAJAX_FUNCTION, 'xAfficherArticleById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerArticle');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierArticle');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverArticle');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherArticles');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherArticlesWCritere');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xOptions');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficheOptions');// On enregistre nos fonctions.
	
	$xajax->register(XAJAX_FUNCTION, 'xAfficherPageById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerPage');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierPage');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverPage');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherPages');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherPagesWCritere');// On enregistre nos fonctions.
	
	$xajax->register(XAJAX_FUNCTION, 'xAfficherUploadById');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xCreerUpload');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xModifierUpload');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xArchiverUpload');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherUpload');// On enregistre nos fonctions.
	$xajax->register(XAJAX_FUNCTION, 'xAfficherUploadWCritere');// On enregistre nos fonctions.

	$xajax->register(XAJAX_FUNCTION, 'xIdentification');	
	$xajax->register(XAJAX_FUNCTION, 'xIdentificationAdmin');
	$xajax->register(XAJAX_FUNCTION, 'xDeconnexion');
	
	$xajax->register(XAJAX_FUNCTION, 'xRequete');
	
	$xajax->register(XAJAX_FUNCTION, 'xRemplirParamSite');
	$xajax->register(XAJAX_FUNCTION, 'xModifierParamSite');
	
	$xajax->processRequest();// Fonction qui va se charger de générer le Javascript, à partir des données que l'on a fournies à xAjax APRÈS AVOIR DÉCLARÉ NOS FONCTIONS.
	$xajax->setLogFile('./bibliotheque/xajax/xajax_errors.log');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
    	<!--
    	Développement : pancard.fr
    	Copyright - Tous droits réservés - à partir de 2013
    	-->
    	<title>nuXt - Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
        <meta name="Author" CONTENT="pancard" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <link href='http://fonts.googleapis.com/css?family=Lato|Sonsie+One|Offside|Baumans|Comfortaa' rel='stylesheet' type='text/css'>
        
        <link rel="shortcut icon" href="./design/images/nuxt.ico" /> 
        
        <link rel="stylesheet" href="./design/css/page.css" />
        <link rel="stylesheet" href="./design/css/menu.css" />
		<link rel="stylesheet" href="./bibliotheque/css/custom-theme/jquery-ui-1.10.3.custom.css" />
		<link rel="stylesheet" href="./bibliotheque/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
		
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<?php $xajax->printJavascript( "./bibliotheque/xajax/" ); ?>
		<script src="./bibliotheque/js/jquery-1.9.1.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/formulaire.js" type="text/javascript"></script>
		<!--<script type="text/javascript" src="./bibliotheque/js/tinymce/tinymce.min.js"></script>-->
		<script type="text/javascript">
			/*tinymce.init({
			    selector: "textarea",
			    language :  "fr_FR" ,
			    plugins: "image",
			    image_advtab: true,
			    plugins: "fullpage",
			    fullpage_default_doctype: "<!DOCTYPE html>",
			    height : 200
			 });
		*/
			$(function() {
    			$( "#dialogCreat" ).dialog({
    			  width:600,
    			  height:400,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openerCreat" ).click(function() {
			      $( "#dialogCreat" ).dialog( "open" );
			    });
			    $( "#closeCreat" ).click(function() {
			      $( "#dialogCreat" ).dialog( "close" );
			    });
		  });
		  
		  $(function() {
    			$( "#dialogOptions" ).dialog({
    			  width:600,
    			  height:400,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openerOptions" ).click(function() {
			      $( "#dialogOptions" ).dialog( "open" );
			    });
			    $( "#closeOptions" ).click(function() {
			      $( "#dialogOptions" ).dialog( "close" );
			    });
		  });
		  
		  $(function() {
    			$( "#dialogCreatGd" ).dialog({
    			  width:1000,
    			  height:600,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openerCreatGd" ).click(function() {
			      $( "#dialogCreatGd" ).dialog( "open" );
			    });
			    $( "#closeCreatGd" ).click(function() {
			      $( "#dialogCreatGd" ).dialog( "close" );
			    });
		  });
		  
		  
		  $(function() {
    			$( "#dialogModif" ).dialog({
    			  width:600,
    			  height:400,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openerModif" ).click(function() {
			      $( "#dialogModif" ).dialog( "open" );
			    });
			    $( "#closeModif" ).click(function() {
			      $( "#dialogModif" ).dialog( "close" );
			    });
		  });
		  
		  $(function() {
    			$( "#dialogModifGd" ).dialog({
    			  width:1000,
    			  height:600,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openerModifGd" ).click(function() {
			      $( "#dialogModifGd" ).dialog( "open" );
			    });
			    $( "#closeModifGd" ).click(function() {
			      $( "#dialogModifGd" ).dialog( "close" );
			    });
		  });
		  
		  function jsIdentificationAdmin(l,p) {
		  	document.getElementById('lbl_resultat').style.opacity='1';
		  	if((l!='')&&(p!='')) {
		  		document.getElementById('lbl_resultat').innerHTML = 'En cours...';
		  		xajax_xIdentificationAdmin(l,p);
		  		var t=setTimeout(function(){window.location.replace('index.php')},3000);
		  		/*if(document.getElementById('check').innerHTML=="ok") {
		  			alert("c'est ok");
			  		window.location.replace('index.php');
			  	}
			  	else {
			  		alert("c'est ko");
			  	}*/
		  	}
		  	else {
		  		document.getElementById('lbl_resultat').innerHTML = 'Les deux champs sont obligatoires';
		  	}
		  	//document.getElementById('check').innerHTML="";
		  }
		  
		  function jsDeconnexion() {
		  	document.getElementById('lbl_resultat').style.opacity='1';
		  	document.getElementById('lbl_resultat').innerHTML = 'En cours...';
		  	xajax_xDeconnexion();
		  	var t=setTimeout(function(){window.location.replace('index.php')},1000);
		  }
		  
		  function jsRequete(t) {
		  	xajax_xRequete(t);
		  }
		  
		  //window.onload=function logFocus() { document.getElementById('login').focus();}

		</script>
		<script src="./bibliotheque/js/users.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/profils.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/categories.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/pages.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/articles.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/menus.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/param.js" type="text/javascript"></script>
		<script src="./bibliotheque/js/upload.js" type="text/javascript"></script>
	</head>
	<body>
	<div id="bandeau_admin">
		<a href="../index.php" class="lien">Visualiser votre site</a>
		<?php
		if(($userLogin!='') && ($droit['1']==1)) {
		?>
			<span class="lien" onClick="jsDeconnexion();">Déconnexion (<? echo $_SESSION['users_login']; ?>)</span>
		<?
		}
		?>
		<span id="lbl_resultat"></span>
	</div>
