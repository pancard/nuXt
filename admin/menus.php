<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>			
<div id="page">
	<h1>Menus</h1>
	<h2>Le menu de votre site. Faites le lien avec les modules ou vos pages</h2>
	<div class="bloc_recherche">
			<form>
				<label for="recherche">Recherche </label><input type="text" id="recherche" name="recherche" value=""  class="text">
				<div class="bouton">
					<input type="reset" value="Effacer" class="reset">
					<input type="button" value="Rechercher" class="submit" onClick="jsAfficherMenusWCritere(document.getElementById('recherche').value);">
				</div>
			</form>
	</div>
	<div class="bloc_ajout">
		<input type="submit" value="Créer" class="submit" id="openerCreat">
	</div>
<!--
	<div id="lbl_resultat"></div>
	-->
		<div class="resultat">
			<div id="dialogCreat" title="Création">
				<form>
					<label for="addnom" id="lbl_nom" class="obligatoire">Nom </label><input type="text" id="addnom" name="addnom" class="text" /><br>
					<label for="addmodule" id="lbl_module">Module </label>
					<div id="addListeModules" class="select">
						<?php
						echo getSelectModules("");
						?>
					</div>	
					<br>
					<input type="button" value="Annuler" class="reset" id="closeCreat">
					<input type="button" value="Créer" class="submit" onClick="jsCreerMenu(document.getElementById('addnom').value,document.getElementById('addmodule').value);">
				</form>
			</div>
			<div id="dialogModif" title="Modification">
				<form>
					<label for="upid" id="lbl_id" > </label><input type="hidden" name="upid" id="upid" /><br>
					<label for="upnom" id="lbl_nom" class="obligatoire">Nom </label><input type="text" id="upnom" name="upnom" class="text" /><br>
					<label for="upmodule" id="lbl_module">Module </label>
					<div id="upListeModules" class="select">
						<!-- Ici les valeurs sont inscrites par une fonction ajax -->
					</div>	
					<br>
					<input type="button" value="Annuler" class="reset" id="closeModif">
					<input type="button" value="Modifier" class="submit" onClick="jsModifierMenu(document.getElementById('upid').value,document.getElementById('upnom').value,document.getElementById('upmodule').value);">					
				</form>
			</div>
			
			<table id="tableMenu">
			<script type="text/javascript">
        		xajax_xAfficherMenus();//On appelle la fonction refresh() pour lancer le script.
            </script>
			</table>
			</div>
	
</div>

<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>