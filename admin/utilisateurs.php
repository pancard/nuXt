<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>			
<div id="page">
	<h1>Utilisateurs</h1>
	<h2>Vos utilisateurs, clients, membres</h2>
	<div class="bloc_recherche">
			<form>
				<label for="recherche">Recherche </label><input type="text" id="recherche" name="recherche" value=""  class="text">
				<div class="bouton">
					<input type="reset" value="Effacer" class="reset">
					<input type="button" value="Rechercher" class="submit" onClick="jsAfficherUsersWCritere(document.getElementById('recherche').value);">
				</div>
			</form>
	</div>
	<div class="bloc_ajout">
		<input type="submit" value="Créer" class="submit" id="openerCreat">
	</div>
<!--
	<div id="lbl_resultat"></div>
	-->
		<div class="resultat">
			<div id="dialogCreat" title="Création">
				<form>
					<label for="addlogin" id="lbl_login" class="obligatoire">Login </label><input type="text" id="addlogin" name="addlogin" class="text" /><br>
					<label for="addpassword" id="lbl_password" class="obligatoire">Password </label><input type="password" id="addpassword" name="addpassword" class="text" /><br>
					<label for="addmail" id="lbl_mail" class="obligatoire">Mail </label><input type="text" id="addmail" name="addmail" class="text" /><br>
					<label for="addavatar" id="lbl_avatar" >Photo </label><input type="file" id="addavatar" name="addavatar" class="file" /><br>
					<label for="addprofil" id="lbl_profil" class="obligatoire">Profils </label>
					<div id="addListeProfils" class="select">
						<?
						echo getSelectProfils("");
						?>
					</div>	
					<br>
					<input type="button" value="Annuler" class="reset" id="closeCreat">
					<input type="button" value="Créer" class="submit" onClick="jsCreerUser(document.getElementById('addlogin').value,document.getElementById('addpassword').value,document.getElementById('addmail').value,document.getElementById('addprofil').value);">
				</form>
			</div>
			<div id="dialogModif" title="Modification">
				<form>
					<label for="upid" id="lbl_id" > </label><input type="hidden" name="upid" id="upid" /><br>
					<label for="uplogin" id="lbl_login" class="obligatoire">Login </label><input type="text" id="uplogin" name="uplogin" class="text" /><br>
					<label for="uppassword" id="lbl_password" class="obligatoire">Password </label><input type="password" id="uppassword" name="uppassword" class="text" /><br>
					<label for="upmail" id="lbl_mail" class="obligatoire">Mail </label><input type="text" id="upmail" name="upmail" class="text" /><br>
					<label for="upavatar" id="lbl_avatar" >Photo </label><input type="file" id="upavatar" name="upavatar" class="file" /><br>
					<label for="upprofil" id="lbl_profil" class="obligatoire">Profils </label>
					<div id="upListeProfils" class="select">
						<!-- Ici les valeurs sont inscrites par une fonction ajax -->
					</div>	
					<br>
					<input type="button" value="Annuler" class="reset" id="closeModif">
					<input type="button" value="Modifier" class="submit" onClick="jsModifierUser(document.getElementById('upid').value,document.getElementById('uplogin').value,document.getElementById('uppassword').value,document.getElementById('upmail').value,document.getElementById('upprofil').value);">					
				</form>
			</div>
			
			<table id="tableUser">
			<script type="text/javascript">
        		xajax_xAfficherUsers();//On appelle la fonction refresh() pour lancer le script.
            </script>
			</table>
			</div>
	
</div>
<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>