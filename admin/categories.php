<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>	
			
<div id="page">
	<h1>Catégories</h1>
	<h2>Vos Catégories pour les articles</h2>
	<div class="bloc_recherche">
			<form>
				<label for="recherche">Recherche </label><input type="text" id="recherche" name="recherche" value=""  class="text">
				<div class="bouton">
					<input type="reset" value="Effacer" class="reset">
					<input type="button" value="Rechercher" class="submit" onClick="jsAfficherCategoriesWCritere(document.getElementById('recherche').value);">
				</div>
			</form>
	</div>
	<div class="bloc_ajout">
		<input type="submit" value="Créer" class="submit" id="openerCreat">
	</div>
<!--
	<div id="lbl_resultat"></div>
	-->
		<div class="resultat">
			<div id="dialogCreat" title="Création">
				<form>
					<label for="addlibelle" id="lbl_libelle" class="obligatoire">Libellé </label><input type="text" id="addlibelle" name="addlibelle" class="text" /><br>
					<br>
					<input type="button" value="Annuler" class="reset" id="closeCreat">
					<input type="button" value="Créer" class="submit" onClick="jsCreerCategorie(document.getElementById('addlibelle').value);">
				</form>
			</div>
			<div id="dialogModif" title="Modification">
				<form>
					<label for="upid" id="lbl_id" > </label><input type="hidden" name="upid" id="upid" /><br>
					<label for="uplibelle" id="lbl_libelle" class="obligatoire">Libellé </label><input type="text" id="uplibelle" name="uplibelle" class="text" /><br>
					<br>
					<input type="button" value="Annuler" class="reset" id="closeModif">
					<input type="button" value="Modifier" class="submit" onClick="jsModifierCategorie(document.getElementById('upid').value,document.getElementById('uplibelle').value);">					
				</form>
			</div>
			
			<table id="tableCategorie">
			<script type="text/javascript">
        		xajax_xAfficherCategories();//On appelle la fonction refresh() pour lancer le script.
            </script>
			</table>
		</div>
	
</div>

<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>