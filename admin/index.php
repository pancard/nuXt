<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>	
<div id="page">
	<h1>Accueil</h1>
	<h2>Vos statistiques</h2>
	<div class="bloc_flottant">
		<a href="articles">
		<span class="chiffre">
			<?
			$a = new articles;
			$nbInv = $a->_getNbByValide(0);
			echo (($a->getNb()) + ($nbInv));
			?>
		</span>
		</a>
		<br>
		<span class="texte">Articles</span><br>
		<?
		if($nbInv>1) { 
			$s = 's'; 
		}
		else { 
			$s = '';
		}
		echo 'dont '.$nbInv.' archivé'.$s;
		?>
	</div>
	<div class="bloc_flottant">
		<a href="categories.php">
		<span class="chiffre">
			<?
			$a = new categories;
			$nbInv = $a->_getNbByValide(0);
			echo (($a->getNb()) + ($nbInv));
			?>
		</span>
		</a>
		<br>
		<span class="texte">Catégories</span><br>
		<?
		if($nbInv>1) { 
			$s = 's'; 
		}
		else { 
			$s = '';
		}
		echo 'dont '.$nbInv.' archivée'.$s;
		?>
	</div>
	<div class="bloc_flottant">
		<a href="utilisateurs.php">
		<span class="chiffre">
			<?
			$a = new users;
			$nbInv = $a->_getNbByValide(0);
			echo (($a->getNb()) + ($nbInv));
			?>
		</span>
		</a>
		<br>
		<span class="texte">Utilisateurs</span><br>
		<?
		if($nbInv>1) { 
			$s = 's'; 
		}
		else { 
			$s = '';
		}
		echo 'dont '.$nbInv.' archivé'.$s;
		?>
	</div>
	<div class="bloc_flottant">
		<a href="profils.php">
		<span class="chiffre">
			<?
			$a = new profils;
			$nbInv = $a->_getNbByValide(0);
			echo (($a->getNb()) + ($nbInv));
			?>
		</span>
		</a>
		<br>
		<span class="texte">Profils</span><br>
		<?
		if($nbInv>1) { 
			$s = 's'; 
		}
		else { 
			$s = '';
		}
		echo 'dont '.$nbInv.' archivé'.$s;
		?>
	</div>
	<div class="bloc_flottant">
		<a href="pages.php">
		<span class="chiffre">
			<?
			$a = new pages;
			$nbInv = $a->_getNbByValide(0);
			echo (($a->getNb()) + ($nbInv));
			?>
		</span>
		</a>
		<br>
		<span class="texte">Pages</span><br>
		<?
		if($nbInv>1) { 
			$s = 's'; 
		}
		else { 
			$s = '';
		}
		echo 'dont '.$nbInv.' archivé'.$s;
		?>
	</div>
	<div class="bloc_flottant">
		<a href="menus.php">
		<span class="chiffre">
			<?
			$a = new menus;
			$nbInv = $a->_getNbByValide(0);
			echo (($a->getNb()) + ($nbInv));
			?>
		</span>
		</a>
		<br>
		<span class="texte">Menus</span><br>
		<?
		if($nbInv>1) { 
			$s = 's'; 
		}
		else { 
			$s = '';
		}
		echo 'dont '.$nbInv.' archivé'.$s;
		?>
	</div>
</div>

<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>