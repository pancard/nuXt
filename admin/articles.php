<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>	
			
<div id="page">
	<h1>Articles</h1>
	<h2>Vos Articles</h2>
	<div class="bloc_recherche">
			<form>
				<label for="recherche">Recherche </label><input type="text" id="recherche" name="recherche" value=""  class="text">
				<div class="bouton">
					<input type="reset" value="Effacer" class="reset">
					<input type="button" value="Rechercher" class="submit" onClick="jsAfficherArticlesWCritere(document.getElementById('recherche').value);">
				</div>
			</form>
	</div>
	<div class="bloc_ajout">
		<input type="submit" value="Créer" class="submit" id="openerCreatGd">&nbsp;
		<input type="submit" value="Options" class="submit" onClick="jsAfficheOptions();">
	</div>
<!--
	<div id="lbl_resultat"></div>
	-->
		<div class="resultat">
			<div id="dialogCreatGd" title="Création">
				<form>
					<label for="addtitre" id="lbl_titre" class="obligatoire">Titre </label><input type="text" id="addtitre" name="addtitre" class="text" /><br>
					<label for="addcategorie" id="lbl_categorie" class="obligatoire">Catégorie </label>
					<div id="addListeCategories" class="select">
						<?
						echo getSelectCategories("");
						?>
					</div>	
					<label for="addcreateur" id="lbl_createur" class="obligatoire">Créateur </label>
					<div id="addListeCreateur" class="select">
						<?
						echo getSelectCreateur("");
						?>
					</div>
					<br>
					<label for="addtexte" id="lbl_texte" class="obligatoire">Texte </label><br>
						<textarea id="addtexte" name="addtexte" class="area" cols="100" rows="10"></textarea><br>
					<label for="addtags" id="lbl_tags">Tags (mots clés séparés par des virgules) </label><br>
						<input type="text" id="addtags" name="addtags" class="text_gd" /><br>
					<label for="addimg" id="lbl_img">Image de présentation (url) </label><br>
						<input type="text" id="addimg" name="addimg" class="text_gd" /><br>
					<br>
					<input type="button" value="Annuler" class="reset" id="closeCreatGd">
					<input type="button" value="Créer" class="submit" onClick="jsCreerArticle(document.getElementById('addtitre').value,document.getElementById('addtexte').value,document.getElementById('addcategorie').value,document.getElementById('addcreateur').value,document.getElementById('addtags').value,document.getElementById('addimg').value);">
				</form>
			</div>
			<div id="dialogModifGd" title="Modification">
				<form>
					<label for="upid" id="lbl_id" > </label><input type="hidden" name="upid" id="upid" /><input type="hidden" name="upidCat" id="upidCat" /><input type="hidden" name="upidCre" id="upidCre" /><br>
					<label for="uptitre" id="lbl_titre" class="obligatoire">Titre </label><input type="text" id="uptitre" name="uptitre" class="text" /><br>
					<label for="upcategorie" id="lbl_categorie" class="obligatoire">Catégorie </label>
					<div id="upListeCategories" class="select">
						<!-- Ici les valeurs sont inscrites par une fonction ajax -->
					</div>
					<label for="upcreateur" id="lbl_createur" class="obligatoire">Créateur </label>
					<div id="upListeCreateur" class="select">
						<!-- Ici les valeurs sont inscrites par une fonction ajax -->
					</div>
					<br>
					<label for="uptexte" id="lbl_texte" class="obligatoire">Texte </label><br>
						<textarea id="uptexte" name="uptexte" class="area" cols="100" rows="10"></textarea><br>
					<label for="uptags" id="lbl_tags">Tags (mots clés séparés par des virgules) </label><br>
						<input type="text" id="uptags" name="uptags" class="text_gd" /><br>
					<label for="upimg" id="lbl_img">Image de présentation (url) </label><br>
						<input type="text" id="upimg" name="upimg" class="text_gd" /><br>
					<br>
					<input type="button" value="Annuler" class="reset" id="closeModifGd">
					<input type="button" value="Modifier" class="submit" onClick="jsModifierArticle(document.getElementById('upid').value,document.getElementById('uptitre').value,document.getElementById('uptexte').value,document.getElementById('upcategorie').value,document.getElementById('upcreateur').value,document.getElementById('uptags').value,document.getElementById('upimg').value);">					
				</form>
			</div>
			<div id="dialogOptions" title="Options">
				<h3>Article à mettre en avant</h3>
				<label for="art" id="lbl_art">Article </label>
					<div id="listeArticles" class="select">
						<!-- Ici les valeurs sont inscrites par une fonction ajax -->
					</div>
				<br>
				<label for="logoSite" id="lbl_img">Image </label>
					<div id="listeUpload" class="select">
						<!-- Ici les valeurs sont inscrites par une fonction ajax -->
					</div>
				<br>
				<input type="button" value="Annuler" class="reset" id="closeOptions">
				<input type="button" value="Modifier" class="submit" onClick="jsOptions(document.getElementById('art').value,document.getElementById('logoSite').value);">					
			</div>
			
			<table id="tableArticle">
			<script type="text/javascript">
        		xajax_xAfficherArticles();//On appelle la fonction refresh() pour lancer le script.
            </script>
			</table>
			</div>
	
</div>

<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>