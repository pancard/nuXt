<?php
include('include/header.php');

if(($userLogin != '') && ($droit['1']==1)) {
	include('include/menu.php');
?>	
		
<div id="page">
	<h1>Paramètres</h1>
	<h2>Les informations générales sur votre site</h2>
	<div class="resultat">
		<label id="lbl_nom" for="nomSite">Nom du site</label>
			<input type="text" name="nomSite" id="nomSite" class="text"><br>
		<label id="lbl_url" for="urlSite">URL du site</label>
			<input type="text" name="urlSite" id="urlSite" class="text"><br>
		<label id="lbl_slogan" for="sloganSite">Slogan du site</label>
			<input type="text" name="sloganSite" id="sloganSite" class="text_gd"><br>
		<label id="lbl_description" for="desSite">Description du site</label>
			<input type="text" name="desSite" id="desSite" class="text_gd"><br>
		<label id="lbl_logo" for="logoSite">Logo du site</label>
			<div class="select" id="listeLogoSite">
				<!-- Ici les valeurs sont inscrites par une fonction ajax -->
			</div>
			<!--<input type="text" name="logoSite" id="logoSite" class="text_gd">-->
			<br>
			<img src="" name="logoImgSite" id="logoImgSite" alt="Logo" width="256px" height="256px"/>
			<br>
		<label id="lbl_design" for="idDesignSite">Design du site</label>
			<div class="select" id="listeDesignSite">
				<!-- Ici les valeurs sont inscrites par une fonction ajax -->
			</div>
		<br>
		<input type="button" value="Annuler" class="reset">
		<input type="button" value="Modifier" class="submit" onClick="jsModifierParamSite(document.getElementById('nomSite').value,document.getElementById('urlSite').value,document.getElementById('sloganSite').value,document.getElementById('desSite').value,document.getElementById('logoSite').value,document.getElementById('idDesignSite').value);">		
	</div>
	<!--
	<div id="lbl_resultat"></div>
	-->
</div>

<?php
}
else {
	include('include/log.php');
}

include('include/footer.php');
?>