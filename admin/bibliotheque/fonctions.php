<?php
/* FTP */
function getSizeRepFtp() {
	$size = "";
	
	// Mise en place d'une connexion basique
	$conn_id = ftp_connect($GLOBALS['serveurFTP']);

	// Identification avec un nom d'utilisateur et un mot de passe
	$login_result = ftp_login($conn_id, $GLOBALS['loginFTP'], $GLOBALS['pwdFTP']);

	// Je boucle sur chaque fichier du répertoire alloué au site
	// Récupération du contenu d'un dossier
	//$size .= $GLOBALS['nomClient'].$GLOBALS['repFTP'].'<br><br>';
	$contents = ftp_nlist($conn_id, $GLOBALS['nomClient'].$GLOBALS['repFTP']);
	//$size .= "nb fichiers : ".count($contents)."<br>";
	for($i=2;$i<count($contents);$i++) {
		// Récupération de la taille du fichier $file
		$size += (ftp_size($conn_id, $contents[$i]) / 1048576);
		//$size .= "fichier ".$i." : ".$contents[$i]."<br>";
	}

	if ($size == -1) {
    	$size = "Impossible de récupérer la taille du répertoire";
	}

	// Fermeture de la connexion
	ftp_close($conn_id);
	
	return round($size,2);
}

function datefr($date) { 
	$split = split("-",$date); 
	$annee = $split[0]; 
	$mois = $split[1]; 
	$jour = $split[2];
	switch ($mois) {
		case "01" : $mois = "janvier";break;
		case "02" : $mois = "février";break;
		case "03" : $mois = "mars";break;
		case "04" : $mois = "avril";break;
		case "05" : $mois = "mai";break;
		case "06" : $mois = "juin";break;
		case "07" : $mois = "juillet";break;
		case "08" : $mois = "août";break;
		case "09" : $mois = "septembre";break;
		case "10" : $mois = "octobre";break;
		case "11" : $mois = "novembre";break;
		case "12" : $mois = "décembre";break;
	}
	
	return "$jour"." "."$mois"." "."$annee"; 
} 

function heurefr($heure) {
	return substr_replace(substr_replace($heure,'h',2,1),"",5,3);
}

function sql($str) {
	$result = mysql_query($str);
	if (!$result) {
		$message = '<div class="error">';
		$message .= 'Erreur SQL : ' . mysql_error() . "<br>\n";
		$message .= 'SQL string : ' . $str . "<br>\n";
		$message .= "Merci d'envoyer ce message au webmaster";
		$message .= "</div>";
		die($message);
	}
	return $result;
}

function bbCode($texte) {
    // Mise en forme du texte
    $texte = preg_replace('#\[b\](.+)\[/b\]#', '<b>$1</b>', $texte);
    $texte = preg_replace('#\[s\](.+)\[/s\]#', '<u>$1</u>', $texte);
    $texte = preg_replace('#\[i\](.+)\[/i\]#', '<i>$1</i>', $texte);
    /*$texte = preg_replace('#`\[center\](.+)\[/center\]#isU','<span style="text-align: center;">$1</span>',$texte);*/
    /*$texte = preg_replace('#`\[center\](.+)\[/center\]#isU','<center>$1</center>',$texte);*/
    $texte = preg_replace('#\[color=(red|green|blue|yellow|purple|olive|silver|grey|navy|\#[A-Za-z0-9]{6})\](.+)\[/color\]#isU', '<span style="color:$1">$2</span>', $texte);
    $texte = preg_replace('#\[a\](.+)\[/a\]#','<a href="$1" target="_blank">$1</a>',$texte);
    /*$texte = preg_replace('#http://[a-z0-9._/-]+#', '<a href="$0" target="_blank">$0</a>', $texte);*/
    $texte = preg_replace('#\[img\](.+)\[/img\]#', '<img src="$1" alt="Image" class="image_article" />', $texte);
    $texte = preg_replace('#\[url\](.+)\[/url\]#', '<a href="$1" class="lien_article" >$1</a>', $texte);
    $texte = nl2br($texte);
     
    // Mise en place des smileys
    $texte = str_replace(':)', '<img src="'.$GLOBALS['smileys'].'souriant.png" title="souriant" alt=":)" class="smiley"/>', $texte);
    $texte = str_replace('=)', '<img src="'.$GLOBALS['smileys'].'souriant.png" title="souriant" alt="=)" class="smiley"/>', $texte);
    $texte = str_replace('^^', '<img src="'.$GLOBALS['smileys'].'content.png" title="content" alt="^^" class="smiley"/>', $texte);
    $texte = str_replace('lol', '<img src="'.$GLOBALS['smileys'].'mortDeRire.png" title="lol" alt="lol" class="smiley"/>', $texte);
    $texte = str_replace(':(', '<img src="'.$GLOBALS['smileys'].'triste.png" title="triste" alt=":(" class="smiley"/>', $texte);
    $texte = str_replace(';)', '<img src="'.$GLOBALS['smileys'].'complice.png" title="complice" alt=";)" class="smiley"/>', $texte);
    $texte = str_replace(':D', '<img src="'.$GLOBALS['smileys'].'lol.png" title="rire" alt=":D" class="smiley"/>', $texte);
    $texte = str_replace(':s', '<img src="'.$GLOBALS['smileys'].'confus.png" title="confus" alt=":s" class="smiley"/>', $texte);
    $texte = str_replace(':O', '<img src="'.$GLOBALS['smileys'].'surpris.png" title="surpris" alt=":O" class="smiley"/>', $texte);
    $texte = str_replace(':|', '<img src="'.$GLOBALS['smileys'].'bof.png" title=":|" alt=":|" class="smiley"/>', $texte);
    $texte = str_replace('%)', '<img src="'.$GLOBALS['smileys'].'fou.png" title="fou" alt="%)" class="smiley"/>', $texte);
    $texte = str_replace(':pleure:', '<img src="'.$GLOBALS['smileys'].'malheureux.png" title="malheureux" alt=":pleure:" class="smiley"/>', $texte);
    $texte = str_replace('mdr', '<img src="'.$GLOBALS['smileys'].'mortDeRire.png" title="mort de rire" alt="mdr" class="smiley"/>', $texte);
     
    return $texte;
}

function string2url($chaine) { 
	$chaine = trim($chaine); 
	$chaine = strtr($chaine, 
	"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ", 
	"aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn"); 
 	$chaine = strtr($chaine,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz"); 
	$chaine = preg_replace('#([^.a-z0-9]+)#i', '-', $chaine); 
	$chaine = preg_replace('#-{2,}#','-',$chaine); 
	$chaine = preg_replace('#-$#','',$chaine); 
	$chaine = preg_replace('#^-#','',$chaine); 
	return $chaine; 
}

function securite_bdd($string) {
        // On regarde si le type de string est un nombre entier (int)
        if(ctype_digit($string))
        {
            $string = intval($string);
        }
        // Pour tous les autres types
        else
        {
            $string = mysql_real_escape_string($string);
            $string = addcslashes($string, '%_');
            $string = addslashes($string);
            $string = stripslashes($string);
            $string = strip_tags($string);
            
        }
         
        return $string;
    }

function lireBdd($string,$inputBool) {
	//return stripslashes(htmlspecialchars($string));
	if($inputBool) {
		return stripslashes(strip_tags($string));
	}
	else {
		return bbCode(stripslashes(nl2br(strip_tags($string))));
	}
}

function getNextID($table) {
	$nb = 1;
	
	try {
		connexion();
		$sql = sql('SELECT max('.$table.'_id) + 1 as id FROM nuxt_'.$table.';');
	
		while($ligne=mysql_fetch_array($sql)) {
			$nb = $ligne['id'];
		}
		
		if($nb == null) {
			$nb = 1;
		}
	
		sql('ALTER TABLE nuxt_'.$table.' AUTO_INCREMENT='.$nb);
	
		deconnexion();
	}
	catch (Exception $e) {
		$nb = 1;
	}
	
	return $nb;
}

function xIdentification($login,$password) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_users WHERE users_login = "'.$login.'" AND users_password = "'.$password.'" ;');
		deconnexion();

		$msg = 'Utilisateur inconnu...';
			
		while($ligne = mysql_fetch_array($repSql)) {
			if($ligne['users_valide'] == '0') {
				$msg = 'Ce compte est désactivé, contacter le support pancard ° <a href="mailto:support@pancard.fr">support@pancard.fr</a>';

			}
			else {
				$msg = 'Connexion réussie, veuillez patienter...';
				// je renseigne la variable global (cookie ou session) et je réactualise la page
				$_SESSION['users_id']=$ligne['users_id'];
				$_SESSION['users_login']=$ligne['users_login'];
				$_SESSION['users_profil_id']=$ligne['users_profil_id'];
				$_SESSION['users_avatar']=$ligne['users_avatar'];
			}
		}
		
		$reponse->assign('lbl_resultat','innerHTML',$msg);

		return $reponse;
}

function xIdentificationAdmin($login,$password) {
		$reponse = new xajaxResponse();	
		
		connexion();
		$repSql = sql('SELECT * FROM nuxt_users WHERE users_login = "'.$login.'" AND users_password = "'.$password.'" ;');
		deconnexion();

		$msg = 'Utilisateur inconnu...';
			
		while($ligne = mysql_fetch_array($repSql)) {
			if($ligne['users_valide'] == 0) {
				$msg = 'Ce compte est désactivé, contacter le support pancard ° <a href="mailto:support@pancard.fr" class="lien">support@pancard.fr</a>';

			}
			else {
				$droit = getDroitsUsers($ligne['users_id']);
				if($droit['1']==1) {
					$msg = 'Connexion réussie, veuillez patienter...';
					// je renseigne la variable global (cookie ou session) et je réactualise la page
					$_SESSION['users_id']=$ligne['users_id'];
					$_SESSION['users_login']=$ligne['users_login'];
					$_SESSION['users_profil_id']=$ligne['users_profil_id'];
					$_SESSION['users_avatar']=$ligne['users_avatar'];
				}
				else {
					$msg = "Vous n'êtes pas autorisé à accéder à cette zone";
				}
			}
		}
		
		$reponse->assign('lbl_resultat','innerHTML',$msg);

		return $reponse;
}

function xDeconnexion() {
	$reponse = new xajaxResponse();
	
	// Je vide toute les sessions admin
	$_SESSION['users_login'] = '';
	$_SESSION['users_password'] = '';
	$_SESSION['users_profil_id'] = '';
	session_destroy();

	$reponse->assign('lbl_resultat','innerHTML','Déconnexion réussie, veuillez patienter...');
		
	return $reponse;
}

function xRequete($texte) {
	$reponse = new xajaxResponse();
	
	connexion();
	$requete = sql($texte);
	deconnexion();
	
	while($ligne = mysql_fetch_array($requete)) {
		$res .= $ligne;
	}

	$reponse->assign('resultat_req','innerHTML','Requête exécutée<br><br>'.$texte.'<br><br>'.$res);
				
	return $reponse;
}

function xRecherche($texte) {
	$reponse = new xajaxResponse();
	
	$_SESSION['dem_recherche'] = "";
	$_SESSION['res_recherche'] = "";
	
	//RESTE A FAIRE : gérer les quotes
	
	$res = "";
	
	connexion();
	// je recherche dans les articles et pages uniquement pour le moment
	$requete = sql("SELECT * FROM nuxt_articles WHERE articles_valide = 1 AND (articles_titre like '%".$texte."%' OR articles_texte like '%".$texte."%' OR articles_tags like '%".$texte."%');");
	
	$requete_p = sql("SELECT * FROM nuxt_pages WHERE pages_valide = 1 AND (pages_titre like '%".$texte."%' OR pages_texte like '%".$texte."%');");
	deconnexion();
	
	$res .= "<h2>Résultats dans les articles</h2>";
			
	while($ligne = mysql_fetch_array($requete)) {
		$res .= "<p><a href='articles.php?id=".$ligne['articles_id']."' class='lien'>".$ligne['articles_titre']."</a></p>";
	}
	
	$res .= "<h2>Résultats dans les pages</h2>";
	
	while($ligne = mysql_fetch_array($requete_p)) {
		$res .= "<p><a href='pages.php?id=".$ligne['pages_id']."' class='lien'>".$ligne['pages_titre']."</a></p>";
	}
	
	$_SESSION['res_recherche'] = $res;
	$_SESSION['dem_recherche'] = 'Votre recherche : '.$texte;
	
	$reponse->assign('dem_recherche','innerHTML','Votre recherche : '.$texte);
	$reponse->assign('res_recherche','innerHTML',$res);
				
	return $reponse;
}




?>