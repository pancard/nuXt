$(function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "blind",
        duration: 1000
      }
    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
    
        
    $( "#message" ).dialog({
      autoOpen: true,
      show: {
        effect: "slide",
        duration: 1000
      },
      hide: {
        effect: "slide",
        duration: 1000
      }
    });
  });