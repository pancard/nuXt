<?php
include('./include/header.php');
include('./include/haut.php');
?>

<?
$idQuestion = $_GET['id'];
$idCategorie = $_GET['cat'];
?>
<div id="corps">	
		<?
		if($idQuestion!='') {
			echo getQuestions($idQuestion,$userId,'');
		}
		else if($idCategorie!='') {
		?>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les catégories...</span>
				</div>	
			</div>
			<div class="contenu">
				<div class="blocs">
					<?
					// J'affiche d'abord les catégories puis les derniers articles puis tout les articles
					echo getCategoriesQuestions($idCategorie);
					?>
				</div>
			</div>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les questions...</span>
				</div>
			</div>
			<?
			echo getQuestions($idQuestion,$userId,$idCategorie);
		}
		else {
		?>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les catégories...</span>
				</div>	
			</div>
			<div class="contenu">
				<div class="blocs">
					<?
					// J'affiche d'abord les catégories puis les derniers articles puis tout les articles
					echo getCategoriesQuestions($idCategorie);
					?>
				</div>
			</div>
			<!--
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les dernières questions...</span>
				</div>
			</div>
			<div class="contenu">
				<div class="blocs">
					<?php
					echo getDebutQuestions(20);
					?>
				</div>
			</div>
			-->
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les questions...</span>
				</div>
			</div>
			<?
			echo getQuestions($idQuestion,$userId,'');
			?>
			
		<?
		}
		?>
		<div id="situation">
			<!-- Où l'utilisateur se trouve sur le site -->
		</div>
	<?
	include('./include/aside.php');
	?>
</div>

<?php
include('./include/footer.php');
?>