<?php
include('include/header.php');
include('include/haut.php');
?>

<div id="corps">
	<div class="contenu">
		<?php
		echo getMiseEnAvantArticle(1);
		?>
		<div class="texteBandeau">
			<span class='titre'>Les derniers articles...</span>
		</div>
		<div class="blocs">
			<?php
			echo getDebutArticles(20);
			?>
		</div>
	</div>

	<?
	include('include/aside.php');
	?>
</div>

<?php
include('include/footer.php');
?>