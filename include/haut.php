<?php
// Si l'utilisateur est connecté, qu'il a un profil lui permettant d'accéder à l'administration, j'affiche la div
if($userLogin != '') {
	if($droit['1']==1) {
	?>
		<div id="bandeau_admin">
			<a href="/admin/" class="lien">Administration</a>
			<span id="lbl_resultat"></span>
		</div>
	<?
	}
	else {
	?>
		<div id="bandeau_admin">
			<span id="lbl_resultat"></span>
		</div>
	<?
	}
}
else {
?>
	<div id="bandeau_admin">
		<span id="lbl_resultat"><?php echo datefr(date()); ?></span>
	</div>
<?
}
?>
<div id="fleche_haut">
	<a href="#onestenhaut">
		<img src="<? echo $GLOBALS['repImg']; ?>up-arrow.png" alt="Remonter" />
	</a>
</div>
<div id="haut">
	<div class="logo">
		<a href="<?php echo $GLOBALS['url']; ?>" class="none">
			<img src="<? echo $logo; ?>" alt="Logo" class="image"/>
		</a>
		<a href="<?php echo $GLOBALS['url']; ?>" class="none">
			<span class="titre"><? echo $titre; ?></span>
		</a>
		<br>
		<span class="slogan"><? echo $slogan; ?></span>
	</div>
	<div class="cnx_ins">
		<?php
		if(($userLogin != '')) {
		?>
		Bienvenue&nbsp;
		<img src="<? echo $GLOBALS['avatarChemin'].$userAvatar; ?>" alt="Avatar de <? echo $userLogin; ?>" class="avatar"/>
		<a href="users.php?id=<? echo $userId; ?>" class="lien"><? echo $userLogin; ?></a>
		&nbsp;-&nbsp;
		<span onClick="jsDeconnexion();" class="lien">Déconnexion</span>
		<?
		}
		else { ?>
			<span id="openCnx" class="lien">Connexion</span>&nbsp;-&nbsp;
			<span id="openIns" class="lien">Inscription</span>
		<?
		}
		?>
	</div>
	<div class="menu">
		<ul>
			<?
			echo getMenuSite();
			?>
		</ul>
	</div>
	<div class="recherche">
		<input type="text" class="text" id="rech" name="rech" value="Recherche..." onClick="document.getElementById('rech').value='';" onKeyPress="if (event.keyCode == 13) jsRecherche(document.getElementById('rech').value);"/>
		<img src="<? echo $GLOBALS['repImg']; ?>loupe.png" alt="Lancer la recherche" onClick="jsRecherche(document.getElementById('rech').value);" />
	</div>
	<div id="dialogCnx" title="Connexion">
		<label for="login" name="lbllogin">Identifiant</label>
		<input type="text" name="login" id="login" class="text" onKeyPress="if (event.keyCode == 13) jsIdentification(document.getElementById('login').value,document.getElementById('password').value);"/>
		<br>
		<label for="password" name="lblpassword">Mot de passe</label>
		<input type="password" name="password" id="password" class="text" onKeyPress="if (event.keyCode == 13) jsIdentification(document.getElementById('login').value,document.getElementById('password').value);"/>
		<br><br><br>
		<input type="submit" name="btOkCnx" value="Connexion" class="submit" onClick="jsIdentification(document.getElementById('login').value,document.getElementById('password').value);" />
		<input type="button" name="btAnCnx" class="reset" value="Annuler" id="closeCnx" />
		<br><br><br>
		<span id="messageIdent"></span>
	</div>
	<div id="dialogIns" title="Inscription">
		<label for="login" name="lbllogin">Identifiant</label>
		<input type="text" name="login" id="login" class="text" onKeyPress="if (event.keyCode == 13) jsInscription(document.getElementById('login').value,document.getElementById('password').value,document.getElementById('mail').value);"/>
		<br>
		<label for="password" name="lblpassword">Mot de passe</label>
		<input type="password" name="password" id="password" class="text" onKeyPress="if (event.keyCode == 13) jsInscription(document.getElementById('login').value,document.getElementById('password').value,document.getElementById('mail').value);"/>
		<br>
		<label for="mail" name="lblmail">E-Mail</label>
		<input type="text" name="mail" id="mail" class="text" onKeyPress="if (event.keyCode == 13) jsInscription(document.getElementById('login').value,document.getElementById('password').value,document.getElementById('mail').value);"/>
		<br><br><br>
		<input type="submit" name="btOkIns" value="Connexion" class="submit" onClick="jsInscription(document.getElementById('login').value,document.getElementById('password').value,document.getElementById('mail').value);" />
		<input type="button" name="btAnIns" class="reset" value="Annuler" id="closeIns" />
	</div>
</div>