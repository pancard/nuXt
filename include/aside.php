<div class="aside"> 
	<!-- Derniers articles -->
	<div class="bloc_aside">
		<span class="titre_aside">Derniers Articles</span>
		<?
		echo getLastArticles(5);
		?>
		<hr>
	</div>
	<!-- Catégories -->
	<div class="bloc_aside">
		<span class="titre_aside">Catégories</span>
		<?
		echo getListeCategories();
		?>
		<hr>
	</div>
	<!-- Dernières questions -->
	<div class="bloc_aside">
		<span class="titre_aside">Dernières Questions</span>
		<?
		echo getLastQuestions(5);
		?>
		<hr>
	</div>
	<!-- Catégories des questions
	<div class="bloc_aside">
		<span class="titre_aside">Catégories</span>
		<?
		echo getListeCategoriesQuestions();
		?>
		<hr>
	</div> -->
	<!-- Derniers membres -->
	<div class="bloc_aside">
		<span class="titre_aside">Derniers Membres</span>
		<?
		echo getLastUsers(5);
		?>
		<hr>
	</div>
</div>