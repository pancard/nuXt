<?php
	// On démarre la session AVANT d'écrire du code HTML
	session_start();
	include('global.php');
	// On s'occupe de tout ce qui est xAjax
	require_once('./admin/bibliotheque/xajax/xajax_core/xajax.inc.php');
	$xajax = new xajax(); //On initialise l'objet xajax.
	
	$xajax->register(XAJAX_FUNCTION, 'xIdentification');
	$xajax->register(XAJAX_FUNCTION, 'xDeconnexion');
	
	$xajax->register(XAJAX_FUNCTION, 'xRecherche');
	
	$xajax->register(XAJAX_FUNCTION, 'xLike');
	$xajax->register(XAJAX_FUNCTION, 'xAfficheLikeUnlike');

	$xajax->register(XAJAX_FUNCTION, 'xLikeQuestion');
	$xajax->register(XAJAX_FUNCTION, 'xAfficheLikeUnlikeQuestion');
	
	$xajax->register(XAJAX_FUNCTION, 'xUpdUser');
	
	$xajax->processRequest();// Fonction qui va se charger de générer le Javascript, à partir des données que l'on a fournies à xAjax APRÈS AVOIR DÉCLARÉ NOS FONCTIONS.
	$xajax->setLogFile('./bibliotheque/xajax/xajax_errors.log');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
    	<!--
    	Développement : pancard.fr
    	Copyright - Tous droits réservés - à partir de 2013
    	-->
    	<title><?php echo $titre; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
        <meta name="Author" CONTENT="pancard" />
        <meta name="Description" CONTENT="<? echo $description; ?>" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <link href='http://fonts.googleapis.com/css?family=Lato|Sonsie+One|Offside|Baumans|Comfortaa' rel='stylesheet' type='text/css' />
        
        <link rel="shortcut icon" href="./admin/design/images/nuxt.ico" /> 
        
        <link rel="stylesheet" href="./admin/design/<? echo $design; ?>/style.css" />
		<link rel="stylesheet" href="./admin/bibliotheque/css/custom-theme/jquery-ui-1.10.3.custom.css" />
		<link rel="stylesheet" href="./admin/bibliotheque/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="./admin/bibliotheque/css/<? echo $design; ?>/jquery-ui-1.10.3.custom.css" />
		<link rel="stylesheet" href="./admin/bibliotheque/css/<? echo $design; ?>/jquery-ui-1.10.3.custom.min.css" />
		
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<?php $xajax->printJavascript( "./admin/bibliotheque/xajax/" ); ?>
		<script src="./admin/bibliotheque/js/jquery-1.9.1.js" type="text/javascript"></script>
		<script src="./admin/bibliotheque/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
		<script src="./admin/bibliotheque/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<script src="./admin/bibliotheque/js/formulaire.js" type="text/javascript"></script>
		<script src="./admin/bibliotheque/js/smooth.pack.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
    			$( "#dialogCnx" ).dialog({
    			  width:400,
    			  height:300,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openCnx" ).click(function() {
			      $( "#dialogCnx" ).dialog( "open" );
			    });
			    $( "#closeCnx" ).click(function() {
			      $( "#dialogCnx" ).dialog( "close" );
			    });
		  	});
		  	$(function() {
    			$( "#dialogIns" ).dialog({
    			  width:500,
    			  height:400,
				  modal: true,
			      autoOpen: false,
			      show: {
			        effect: "blind",
			        duration: 1000
			      },
			      hide: {
			        effect: "blind",
			        duration: 1000
			      }
			    });
 
			    $( "#openIns" ).click(function() {
			      $( "#dialogIns" ).dialog( "open" );
			    });
			    $( "#closeIns" ).click(function() {
			      $( "#dialogIns" ).dialog( "close" );
			    });
		  	});
		  
		  	function jsIdentification(l,p,page) {
				document.getElementById('messageIdent').style.opacity='1';
			  	if((l!='')&&(p!='')) {
			  		document.getElementById('messageIdent').innerHTML = 'En cours...';
			  		xajax_xIdentification(l,p);
		  			$( "#dialogCnx" ).dialog( "close" );
			  		var t=setTimeout(function(){window.location.replace('index.php')},1000);
		  		}
		  		else {
		  			document.getElementById('messageIdent').innerHTML = 'Les deux champs sont obligatoires';
		  		}
		  	}
		  
		    function jsDeconnexion() {
		  		xajax_xDeconnexion();
			  	$( "#dialogCnx" ).dialog( "close" );
			  	var t=setTimeout(function(){window.location.replace('index.php')},1000);
			}
		  
		  	function jsRecherche(r) {
		  	//if((r=='Recherche...') || (r=='')) {
		  		
		  	//}
		  	//else {
		  			xajax_xRecherche(r);
		  			var t=setTimeout(function(){window.location.replace('recherche.php')},1000);
		  	//}
		  	}
		  
		  	function jsLike(art,val,user) {
		  		document.getElementById('chargement').style.opacity='1';
				document.getElementById('lbl_resultat').style.opacity='1';
			  	document.getElementById('compteur'+art).innerHTML = '';
			  	xajax_xLike(art,val,user);
		  		document.getElementById('chargement').style.opacity='0';
		  	}
		  	function jsLikeQuestion(quest,val,user) {
		  		document.getElementById('chargement').style.opacity='1';
				document.getElementById('lbl_resultat').style.opacity='1';
			  	document.getElementById('compteur'+quest).innerHTML = '';
			  	xajax_xLikeQuestion(quest,val,user);
		  		document.getElementById('chargement').style.opacity='0';
		  	}
		  	
		  	function jsUpdUser(id,nom,prenom,fixe,port,mail,site,fb,tw,login,pwd) {
		  		//alert('id: ' + id + ' nom : ' + nom + ' prenom ' + prenom + ' fixe ' + fixe + ' portable ' + port + ' mail ' + mail + ' site ' + site + ' facebook ' + fb + ' twitter ' + tw + ' login ' + login + ' password ' + pwd);
		  		document.getElementById('lbl_resultat').innerHTML='Enregistrement en cours...';
		  		document.getElementById('lbl_resultat').style.opacity='1';
		  		xajax_xUpdUser(id,nom,prenom,fixe,port,mail,site,fb,tw,login,pwd);
		  	}
		  
		  	window.onscroll = scroll;
 
		  	function scroll () {
		  		var nVScroll = document.documentElement.scrollTop || document.body.scrollTop;
				if (nVScroll > 200) {
					//alert("scroll event detected! " + window.pageXOffset + " " + window.pageYOffset);
					document.getElementById('fleche_haut').style.opacity='1';
				}
				if (nVScroll < 200) {
					//alert("scroll event detected! " + window.pageXOffset + " " + window.pageYOffset);
					document.getElementById('fleche_haut').style.opacity='0';
				}
			}
		  </script>
	</head>
	<body>
	<a name="onestenhaut"></a>