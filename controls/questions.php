<?php
function xLikeQuestion($idQuestion,$value,$idUser) {
	$reponse = new xajaxResponse();
	
	// je dois d'abord rechercher si le user à déjà voté, si c'est le cas, j'update, sinon j'insère
	connexion();
	$sql = sql("SELECT * FROM ".$GLOBALS['prefixe']."questions LEFT JOIN ".$GLOBALS['prefixe']."lien_questions_users ON l_questions_id = questions_id AND l_users_id = '".$idUser."' WHERE questions_id = '".$idQuestion."' ;");
	
	// Je regarde donc si l'question à des like ou unlike et si le user connecté à déjà liké ou pas
	$dejaLikeOrUnlike = false;
	
	while ($ligne = mysql_fetch_array($sql)) {
		$like = $ligne['questions_like'];
		$unlike = $ligne['questions_unlike'];
		if($ligne['l_users_id']==$idUser) {
			$dejaLikeOrUnlike = true;
			$likeUser = $ligne['l_like'];
			$unlikeUser = $ligne['l_unlike'];
		}
	}
	
	if($dejaLikeOrUnlike) {
		if($likeUser==1 && $value=='like') {
			//si le user a déjà liké et qu'il re-like, je ne fais rien
			$msg = "Vous avez déjà indiqué aimer cette question";
		}
		else if($likeUser==1 && $value=='unlike') {
			//si le user a déjà liké et qu'il clique sur moins alors on update
			sql("UPDATE ".$GLOBALS['prefixe']."lien_questions_users SET l_like = 0, l_unlike = 1 WHERE l_questions_id = ".$idQuestion." AND l_users_id = ".$idUser." ;");
			$like = $like - 1;
			$unlike = $unlike + 1;
			sql("UPDATE ".$GLOBALS['prefixe']."questions SET questions_like = ".$like.", questions_unlike = ".$unlike." WHERE questions_id = ".$idQuestion." ;");
			
			$typeMsg = "<b>-</b>";
			$msg = "Votre ".$typeMsg." a bien été pris en compte";
		}
		if($unlikeUser==1 && $value=='unlike') {
			//si le user a déjà unliké et qu'il re-unlike, je ne fais rien
			$msg = "Vous avez déjà indiqué ne pas aimer cette question";
		}
		else if($unlikeUser==1 && $value=='like') {
			//si le user a déjà unliké et qu'il clique sur plus alors on update
			sql("UPDATE ".$GLOBALS['prefixe']."lien_questions_users SET l_like = 1, l_unlike = 0 WHERE l_questions_id = ".$idQuestion." AND l_users_id = ".$idUser." ;");
			$like = $like + 1;
			$unlike = $unlike - 1;
			sql("UPDATE ".$GLOBALS['prefixe']."questions SET questions_like = ".$like.", questions_unlike = ".$unlike." WHERE questions_id = ".$idQuestion." ;");
			
			$typeMsg = "<b>+</b>";
			$msg = "Votre ".$typeMsg." a bien été pris en compte";
		}
	}
	else {
		// si c'est la première fois qu'il vote, alors on insère
		$l_like = 0;
		$l_unlike = 0;
		if($value=='like') {
			$like = $like + 1;
			$l_like = 1;
			$typeMsg = "<b>+</b>";
		}
		else if($value=='unlike') {
			$unlike = $unlike + 1;
			$l_unlike = 1;
			$typeMsg = "<b>-</b>";
		}
		sql("INSERT INTO ".$GLOBALS['prefixe']."lien_questions_users (l_questions_id,l_users_id,l_like,l_unlike) VALUES ('".$idQuestion."','".$idUser."','".$l_like."','".$l_unlike."');");
		sql("UPDATE ".$GLOBALS['prefixe']."questions SET questions_like = '".$like."', questions_unlike = '".$unlike."' WHERE questions_id = '".$idQuestion."' ;");
		
		$msg = "Votre ".$typeMsg." a bien été pris en compte";
	}
	deconnexion();
	
	$compteur = $like-$unlike;
	if($compteur > 0) {
		$compteur = "+".$compteur;
	}
	
	$reponse->assign('lbl_resultat','innerHTML',$msg);
	$reponse->assign('compteur'.$idQuestion,'innerHTML',$compteur);
	//$reponse->call("xAfficheLikeUnlike('".$idQuestion."','".$idUser."',true,'".$msg."')");
	
	return $reponse;
}

function xAfficheLikeUnlikeQuestion($idQuestion,$idUser,$xajaxOrNot,$msg) {
	$reponse = new xajaxResponse();
	
	$nomDIVid = 'compteur'.$idQuestion;
	
	$reponse->clear($nomDIVid,'innerHTML');
	
	$res = "";
	
	connexion();
	$sql = sql("SELECT * FROM ".$GLOBALS['prefixe']."questions WHERE questions_id = '".$idQuestion."' ;");
	deconnexion();
	
	// Je regarde donc si la question à des like ou unlike et si le user connecté à déjà liké ou pas
	while ($ligne = mysql_fetch_array($sql)) {
			$like = $ligne['questions_like'];
			$unlike = $ligne['questions_unlike'];
	}
	
	$compteur = $like-$unlike;
	if($compteur > 0) {
		$compteur = "+".$compteur;
	}
	
	$res .= "<div class='like'><div class='compteur'><br>
					<span id='".$nomDIVid."'>".$compteur."</span><br>
					<img src='".$GLOBALS['loading']."' alt='Chargement' id='chargement' class='img' />
					</div>";
	if($idUser != '') {
		$res .= 	"<div class='plus' onClick=jsLikeQuestion('".$idQuestion."','like','".$idUser."');>
						<span>+</span>
					</div>
					<div class='moins' onClick=jsLikeQuestion('".$idQuestion."','unlike','".$idUser."');>
						<span>-</span>
					</div>";
	}
	$res .= "</div>";
	
	if($xajaxOrNot) {
		$reponse->assign($nomDIVid,'innerHTML',$compteur);
		$reponse->assign('lbl_resultat','innerHTML',$msg);
		return $reponse;
	}
	else {
		return $res;
	}
}

function getQuestions($id,$idUser,$cat) {
	//$reponse = new xajaxResponse();
	
	if($id!=0) {
		$req = "SELECT * FROM ".$GLOBALS['prefixe']."questions art INNER JOIN ".$GLOBALS['prefixe']."articles_categories cat ON cat.articles_categories_id = art.articles_categories_id INNER JOIN ".$GLOBALS['prefixe']."users ON users_id = art.questions_users_id_createur WHERE art.questions_valide = 1 AND art.questions_id = ".$id." ORDER BY art.questions_date_creation DESC, questions_id DESC ;";
	}
	else if($cat!=''){
		$req = "SELECT * FROM ".$GLOBALS['prefixe']."questions art INNER JOIN ".$GLOBALS['prefixe']."articles_categories cat ON cat.articles_categories_id = art.articles_categories_id INNER JOIN ".$GLOBALS['prefixe']."users ON users_id = art.questions_users_id_createur WHERE art.questions_valide = 1 AND cat.articles_categories_id = ".$cat." ORDER BY art.questions_date_creation DESC, questions_id DESC ;";
	}
	else {
		$req = "SELECT * FROM ".$GLOBALS['prefixe']."questions art INNER JOIN ".$GLOBALS['prefixe']."articles_categories cat ON cat.articles_categories_id = art.articles_categories_id INNER JOIN ".$GLOBALS['prefixe']."users ON users_id = art.questions_users_id_createur WHERE art.questions_valide = 1 ORDER BY art.questions_date_creation DESC, questions_id DESC ;";
	}

	connexion();
	$sql = sql($req);
	deconnexion();
	
	$res .= "<div class='contenu'>
				<table>
					<tr>
						<th>Notes</th>
						<th>Vues</th>
						<th>Réponses</th>
						<th>Catégories : Questions</th>
						<th>Membres</th>
					</tr>";
	
	while ($ligne = mysql_fetch_array($sql)) {
		// on découpe la liste des tags
		$tags = explode(",",lireBdd($ligne['questions_tags'],false));
		$res .= "<tr>
					<td id='like".$ligne['questions_id']."'>"
						.xAfficheLikeUnlikeQuestion($ligne['questions_id'],$idUser,false,'').
					"</td>
					<td class='compteur'>"
						.$ligne['questions_vues'].	
					"</td>
					<td class='compteur'>"
						.$ligne['questions_rep'].	
					"</td>
					<td>"
						."<a href='questions.php?cat=".lireBdd($ligne['articles_categories_id'],false)."'>".lireBdd($ligne['articles_categories_libelle'],false)."</a> : "."<a href='?id=".lireBdd($ligne['questions_id'],false)."' class='lienQuestion'>".lireBdd($ligne['questions_texte'],false)."</a>
						<p>
						Tags { ";
							for($i = 0; $i < count($tags); $i++) {
							$res .= "<span class='lien' onClick=jsRecherche('".trim($tags[$i])."');>#" .trim($tags[$i])."</span> ";
							}
						$res .= "
						 } 
					</p>
					</td>
					<td>"
						."<a href='users.php?id=".lireBdd($ligne['users_id'],false)."'>".lireBdd($ligne['users_login'],false)."</a>".
					"</td>
				</tr>";
	}
	
	$res .= "</table>
			</div>";
	
	return $res;
}

function getLastQuestions($n) {
	connexion();
	$sql = sql("SELECT * FROM nuxt_questions WHERE questions_valide = 1 ORDER BY questions_date_creation DESC, questions_id DESC LIMIT ".$n);
	deconnexion();
	
	$res = "<ul>";
	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<li>
					<a href='questions.php?id=".$ligne['questions_id']."' >".$ligne['questions_texte']."</a>
				</li>";
	}
	$res .= "</ul>";
	
	return $res;
}

function getDebutQuestions($n) {

	connexion();
	$sql = sql("SELECT * FROM nuxt_questions WHERE questions_valide = 1 ORDER BY questions_date_creation DESC, questions_id DESC LIMIT ".$n);
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {	
		if(lireBdd($ligne['questions_image'],false) != '') {
			$res.=	"<div class='bloc2'>";
			$res .= "<img src='".lireBdd($ligne['questions_image'],false)."' alt='".lireBdd($ligne['questions_titre'],false)."' />";
		}	
		else {
			$res.=	"<div class='bloc'>";
		}
		$res .= "<h2>".lireBdd($ligne['questions_titre'],false)."</h2>";
		$res .= "<p>Le ".datefr($ligne['questions_date_creation'])."<br><br>".substr(lireBdd($ligne['questions_texte'],false),0,50)."...</p>";
		$res .= "<a href='questions.php?id=".$ligne['questions_id']."' class='lien'>En savoir +</a>";
		$res .= "</div>";
	}
	
	return $res;
}

function getCategoriesQuestions($idASelectionner) {
	$res = "";
	
	connexion();
	if($idASelectionner!='') {
		$sql = sql("SELECT count(questions_id) as nb, articles_categories_libelle, cat.articles_categories_id as cat_id FROM nuxt_articles_categories cat INNER JOIN nuxt_questions art ON art.articles_categories_id = cat.articles_categories_id WHERE art.questions_valide = 1 GROUP BY articles_categories_libelle ORDER BY art.questions_date_creation DESC  ;");
	}
	else {
		$sql = sql("SELECT count(questions_id) as nb, articles_categories_libelle, cat.articles_categories_id as cat_id FROM nuxt_articles_categories cat INNER JOIN nuxt_questions art ON art.articles_categories_id = cat.articles_categories_id WHERE art.questions_valide = 1 GROUP BY articles_categories_libelle ORDER BY art.questions_date_creation DESC  ;");
	}
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {
		if($idASelectionner==$ligne['cat_id']) {
			$class = "bloc_flottant_selectionne";
		}
		else {
			$class = "bloc_flottant";
		}
		$res .= "<a href='questions.php?cat=".$ligne['cat_id']."' class='lien'>";
		$res .= "<div class='".$class."'>
				<span class='chiffre'>
				".$ligne['nb']."</span><br>
				<span class='texteChiffre'>".$ligne['articles_categories_libelle']."</span>
				</div>";
		$res .= "</a>";
	}
	
	return $res;
}

function getListeCategoriesQuestions() {
	connexion();
	$sql = sql("SELECT count(questions_id) as nb, articles_categories_libelle, cat.articles_categories_id as cat_id FROM nuxt_articles_categories cat INNER JOIN nuxt_questions art ON art.articles_categories_id = cat.articles_categories_id WHERE art.questions_valide = 1 GROUP BY articles_categories_libelle ORDER BY art.questions_date_creation DESC  ;");
	deconnexion();
	
	$res = "<ul>";
	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<li>
					<a href='questions.php?cat=".$ligne['cat_id']."' >".$ligne['articles_categories_libelle']."</a> (".$ligne['nb'].")
				</li>";
	}
	$res .= "</ul>";
	
	return $res;
}


?>