<?php
function getInfosSite() {
	connexion();
	$sql = sql("SELECT * FROM ".$GLOBALS['prefixe']."param INNER JOIN ".$GLOBALS['prefixe']."design ON design_id = param_design_id_site LEFT JOIN ".$GLOBALS['prefixe']."upload ON upload_id = param_upload_id;");
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {
		$res['titreSite'] = $ligne['param_nom_site'];
		$res['sloganSite'] = $ligne['param_slogan_site'];
		$res['desSite'] = $ligne['param_description_site'];
		$res['mailSite'] = $ligne['param_mail_contact_site'];
		$res['designChemin'] = $ligne['design_chemin'];
		$res['designLogo'] = $ligne['design_logo_defaut'];
		$res['logoSite'] = $ligne['upload_chemin'];
	}
	
	return $res;
}

?>