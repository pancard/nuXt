<?php
function xLike($idArticle,$value,$idUser) {
	$reponse = new xajaxResponse();
	
	// je dois d'abord rechercher si le user à déjà voté, si c'est le cas, j'update, sinon j'insère
	connexion();
	$sql = sql("SELECT * FROM ".$GLOBALS['prefixe']."articles LEFT JOIN ".$GLOBALS['prefixe']."lien_articles_users ON l_articles_id = articles_id AND l_users_id = '".$idUser."' WHERE articles_id = '".$idArticle."' ;");
	
	// Je regarde donc si l'article à des like ou unlike et si le user connecté à déjà liké ou pas
	$dejaLikeOrUnlike = false;
	
	while ($ligne = mysql_fetch_array($sql)) {
		$like = $ligne['articles_like'];
		$unlike = $ligne['articles_unlike'];
		if($ligne['l_users_id']==$idUser) {
			$dejaLikeOrUnlike = true;
			$likeUser = $ligne['l_like'];
			$unlikeUser = $ligne['l_unlike'];
		}
	}
	
	if($dejaLikeOrUnlike) {
		if($likeUser==1 && $value=='like') {
			//si le user a déjà liké et qu'il re-like, je ne fais rien
			$msg = "Vous avez déjà indiqué aimer cet article";
		}
		else if($likeUser==1 && $value=='unlike') {
			//si le user a déjà liké et qu'il clique sur moins alors on update
			sql("UPDATE ".$GLOBALS['prefixe']."lien_articles_users SET l_like = 0, l_unlike = 1 WHERE l_articles_id = ".$idArticle." AND l_users_id = ".$idUser." ;");
			$like = $like - 1;
			$unlike = $unlike + 1;
			sql("UPDATE ".$GLOBALS['prefixe']."articles SET articles_like = ".$like.", articles_unlike = ".$unlike." WHERE articles_id = ".$idArticle." ;");
			
			$typeMsg = "<b>-</b>";
			$msg = "Votre ".$typeMsg." a bien été pris en compte";
		}
		if($unlikeUser==1 && $value=='unlike') {
			//si le user a déjà unliké et qu'il re-unlike, je ne fais rien
			$msg = "Vous avez déjà indiqué ne pas aimer cet article";
		}
		else if($unlikeUser==1 && $value=='like') {
			//si le user a déjà unliké et qu'il clique sur plus alors on update
			sql("UPDATE ".$GLOBALS['prefixe']."lien_articles_users SET l_like = 1, l_unlike = 0 WHERE l_articles_id = ".$idArticle." AND l_users_id = ".$idUser." ;");
			$like = $like + 1;
			$unlike = $unlike - 1;
			sql("UPDATE ".$GLOBALS['prefixe']."articles SET articles_like = ".$like.", articles_unlike = ".$unlike." WHERE articles_id = ".$idArticle." ;");
			
			$typeMsg = "<b>+</b>";
			$msg = "Votre ".$typeMsg." a bien été pris en compte";
		}
	}
	else {
		// si c'est la première fois qu'il vote, alors on insère
		$l_like = 0;
		$l_unlike = 0;
		if($value=='like') {
			$like = $like + 1;
			$l_like = 1;
			$typeMsg = "<b>+</b>";
		}
		else if($value=='unlike') {
			$unlike = $unlike + 1;
			$l_unlike = 1;
			$typeMsg = "<b>-</b>";
		}
		sql("INSERT INTO ".$GLOBALS['prefixe']."lien_articles_users (l_articles_id,l_users_id,l_like,l_unlike) VALUES ('".$idArticle."','".$idUser."','".$l_like."','".$l_unlike."');");
		sql("UPDATE ".$GLOBALS['prefixe']."articles SET articles_like = '".$like."', articles_unlike = '".$unlike."' WHERE articles_id = '".$idArticle."' ;");
		
		$msg = "Votre ".$typeMsg." a bien été pris en compte";
	}
	deconnexion();
	
	$compteur = $like-$unlike;
	if($compteur > 0) {
		$compteur = "+".$compteur;
	}
	
	$reponse->assign('lbl_resultat','innerHTML',$msg);
	$reponse->assign('compteur'.$idArticle,'innerHTML',$compteur);
	//$reponse->call("xAfficheLikeUnlike('".$idArticle."','".$idUser."',true,'".$msg."')");
	
	return $reponse;
}

function xAfficheLikeUnlike($idArticle,$idUser,$xajaxOrNot,$msg) {
	$reponse = new xajaxResponse();
	
	$nomDIVid = 'compteur'.$idArticle;
	
	$reponse->clear($nomDIVid,'innerHTML');
	
	$res = "";
	
	connexion();
	$sql = sql("SELECT * FROM ".$GLOBALS['prefixe']."articles WHERE articles_id = '".$idArticle."' ;");
	deconnexion();
	
	// Je regarde donc si l'article à des like ou unlike et si le user connecté à déjà liké ou pas
	while ($ligne = mysql_fetch_array($sql)) {
			$like = $ligne['articles_like'];
			$unlike = $ligne['articles_unlike'];
	}
	
	$compteur = $like-$unlike;
	if($compteur > 0) {
		$compteur = "+".$compteur;
	}
	
	$res .= "<div class='like'><div class='compteur'><br>
					<span id='".$nomDIVid."'>".$compteur."</span><br>
					<img src='".$GLOBALS['loading']."' alt='Chargement' id='chargement' class='img' />
					</div>";
	if($idUser != '') {
		$res .= 	"<div class='plus' onClick=jsLike('".$idArticle."','like','".$idUser."');>
						<span>+</span>
					</div>
					<div class='moins' onClick=jsLike('".$idArticle."','unlike','".$idUser."');>
						<span>-</span>
					</div>";
	}
	$res .= "</div>";
	
	if($xajaxOrNot) {
		$reponse->assign($nomDIVid,'innerHTML',$compteur);
		$reponse->assign('lbl_resultat','innerHTML',$msg);
		return $reponse;
	}
	else {
		return $res;
	}
}

function getArticles($id,$idUser,$cat) {
	//$reponse = new xajaxResponse();
	
	if($id!=0) {
		$req = "SELECT * FROM ".$GLOBALS['prefixe']."articles art INNER JOIN ".$GLOBALS['prefixe']."articles_categories cat ON cat.articles_categories_id = art.articles_categories_id INNER JOIN ".$GLOBALS['prefixe']."users ON users_id = art.articles_users_id_createur WHERE art.articles_valide = 1 AND art.articles_id = ".$id." ORDER BY art.articles_date_creation DESC, articles_id DESC ;";
	}
	else if($cat!=''){
		$req = "SELECT * FROM ".$GLOBALS['prefixe']."articles art INNER JOIN ".$GLOBALS['prefixe']."articles_categories cat ON cat.articles_categories_id = art.articles_categories_id INNER JOIN ".$GLOBALS['prefixe']."users ON users_id = art.articles_users_id_createur WHERE art.articles_valide = 1 AND cat.articles_categories_id = ".$cat." ORDER BY art.articles_date_creation DESC, articles_id DESC ;";
	}
	else {
		$req = "SELECT * FROM ".$GLOBALS['prefixe']."articles art INNER JOIN ".$GLOBALS['prefixe']."articles_categories cat ON cat.articles_categories_id = art.articles_categories_id INNER JOIN ".$GLOBALS['prefixe']."users ON users_id = art.articles_users_id_createur WHERE art.articles_valide = 1 ORDER BY art.articles_date_creation DESC, articles_id DESC ;";
	}

	connexion();
	$sql = sql($req);
	deconnexion();
	
	while ($ligne = mysql_fetch_array($sql)) {
		// on découpe la liste des tags
		$tags = explode(",",lireBdd($ligne['articles_tags'],false));
		//$reponse->call(xAfficheLikeUnlike($ligne['articles_id'],$idUser));
		$res .= "<div id='like".$ligne['articles_id']."'>
				".xAfficheLikeUnlike($ligne['articles_id'],$idUser,false,'')."
				</div>
				<div class='contenu'>
					<h1><a href='?id=".lireBdd($ligne['articles_id'],false)."' class='lienArticle'>".lireBdd($ligne['articles_titre'],false)."</a></h1>
					<h2>Catégorie : <a href='articles.php?cat=".lireBdd($ligne['articles_categories_id'],false)."'>".lireBdd($ligne['articles_categories_libelle'],false)."</a></h2>
					<div class='infoArticle'>
						<img src='".$GLOBALS['avatarChemin'].$ligne['users_avatar']."' alt='Avatar de ".lireBdd($ligne['users_login'],false)."' class='img'/><br>
						<span class='createurArticle'><a href='users.php?id=".lireBdd($ligne['users_id'],false)."'>".lireBdd($ligne['users_login'],false)."</a></span><br>
						<span class='dateArticle'>".datefr($ligne['articles_date_creation'])."</span>
					</div>
					<p>
						".lireBdd($ligne['articles_texte'],false)."
					</p>
					<br>
					<p>
						Tags { ";
							for($i = 0; $i < count($tags); $i++) {
							$res .= "<span class='lien' onClick=jsRecherche('".trim($tags[$i])."');>#" .trim($tags[$i])."</span> ";
							}
						$res .= "
						 } 
					</p>
				</div>";
	}
	
	return $res;
}

function getLastArticles($n) {
	connexion();
	$sql = sql("SELECT * FROM nuxt_articles WHERE articles_valide = 1 ORDER BY articles_date_creation DESC, articles_id DESC LIMIT ".$n);
	deconnexion();
	
	$res = "<ul>";
	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<li>
					<a href='articles.php?id=".$ligne['articles_id']."' >".$ligne['articles_titre']."</a>
				</li>";
	}
	$res .= "</ul>";
	
	return $res;
}

function getDebutArticles($n) {

	connexion();
	$sql = sql("SELECT * FROM nuxt_articles WHERE articles_valide = 1 ORDER BY articles_date_creation DESC, articles_id DESC LIMIT ".$n);
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {	
		if(lireBdd($ligne['articles_image'],false) != '') {
			$res.=	"<div class='bloc2'>";
			$res .= "<img src='".lireBdd($ligne['articles_image'],false)."' alt='".lireBdd($ligne['articles_titre'],false)."' />";
		}	
		else {
			$res.=	"<div class='bloc'>";
		}
		$res .= "<h2>".lireBdd($ligne['articles_titre'],false)."</h2>";
		$res .= "<p>Le ".datefr($ligne['articles_date_creation'])."<br><br>".substr(lireBdd($ligne['articles_texte'],false),0,50)."...</p>";
		$res .= "<a href='articles.php?id=".$ligne['articles_id']."' class='lien'>En savoir +</a>";
		$res .= "</div>";
	}
	
	return $res;
}

function getCategoriesArticles($idASelectionner) {
	$res = "";
	
	connexion();
	if($idASelectionner!='') {
		$sql = sql("SELECT count(articles_id) as nb, articles_categories_libelle, cat.articles_categories_id as cat_id FROM nuxt_articles_categories cat INNER JOIN nuxt_articles art ON art.articles_categories_id = cat.articles_categories_id WHERE art.articles_valide = 1 GROUP BY articles_categories_libelle ORDER BY art.articles_date_creation DESC  ;");
	}
	else {
		$sql = sql("SELECT count(articles_id) as nb, articles_categories_libelle, cat.articles_categories_id as cat_id FROM nuxt_articles_categories cat INNER JOIN nuxt_articles art ON art.articles_categories_id = cat.articles_categories_id WHERE art.articles_valide = 1 GROUP BY articles_categories_libelle ORDER BY art.articles_date_creation DESC  ;");
	}
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {
		if($idASelectionner==$ligne['cat_id']) {
			$class = "bloc_flottant_selectionne";
		}
		else {
			$class = "bloc_flottant";
		}
		$res .= "<a href='articles.php?cat=".$ligne['cat_id']."' class='lien'>";
		$res .= "<div class='".$class."'>
				<span class='chiffre'>
				".$ligne['nb']."</span><br>
				<span class='texteChiffre'>".$ligne['articles_categories_libelle']."</span>
				</div>";
		$res .= "</a>";
	}
	
	return $res;
}

function getListeCategories() {
	connexion();
	$sql = sql("SELECT count(articles_id) as nb, articles_categories_libelle, cat.articles_categories_id as cat_id FROM nuxt_articles_categories cat INNER JOIN nuxt_articles art ON art.articles_categories_id = cat.articles_categories_id WHERE art.articles_valide = 1 GROUP BY articles_categories_libelle ORDER BY art.articles_date_creation DESC  ;");
	deconnexion();
	
	$res = "<ul>";
	while($ligne = mysql_fetch_array($sql)) {
		$res .= "<li>
					<a href='articles.php?cat=".$ligne['cat_id']."' >".$ligne['articles_categories_libelle']."</a> (".$ligne['nb'].")
				</li>";
	}
	$res .= "</ul>";
	
	return $res;
}

function getMiseEnAvantArticle($nb) {
	$res = "";
	
	connexion();
	$sql = sql("SELECT * FROM nuxt_articles INNER JOIN nuxt_upload ON upload_id = articles_upload_id WHERE articles_miseenavant = 1 AND articles_valide = 1 LIMIT ".$nb.";");
	deconnexion();
	
	while($ligne = mysql_fetch_array($sql)) {
	$res .= "
		<a href='articles.php?id=".$ligne['articles_id']."'>
		<div class='miseEnAvant'>
			<div class='texte'><span class='titre'>".$ligne['articles_titre']."...</span></div>
			<img src='".$GLOBALS['uploadChemin'].$ligne['upload_chemin']."' alt='Mise en avant'/>
		</div>
		</a>
		";
	}
	
	return $res;
}

?>