<?php
include('./include/header.php');
include('./include/haut.php');
?>

<?
$idArticle = $_GET['id'];
$idCategorie = $_GET['cat'];
?>
<div id="corps">	
		<?
		if($idArticle!='') {
			echo getArticles($idArticle,$userId,'');
		}
		else if($idCategorie!='') {
		?>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les catégories...</span>
				</div>	
			</div>
			<div class="contenu">
				<div class="blocs">
					<?
					// J'affiche d'abord les catégories puis les derniers articles puis tout les articles
					echo getCategoriesArticles($idCategorie);
					?>
				</div>
			</div>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les articles...</span>
				</div>
			</div>
			<?
			echo getArticles($idArticle,$userId,$idCategorie);
		}
		else {
		?>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les catégories...</span>
				</div>	
			</div>
			<div class="contenu">
				<div class="blocs">
					<?
					// J'affiche d'abord les catégories puis les derniers articles puis tout les articles
					echo getCategoriesArticles($idCategorie);
					?>
				</div>
			</div>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les 6 derniers articles...</span>
				</div>
			</div>
			<div class="contenu">
				<div class="blocs">
					<?php
					echo getDebutArticles(6);
					?>
				</div>
			</div>
			<div class="contenu">
				<div class="texteBandeau">
					<span class='titre'>Les articles...</span>
				</div>
			</div>
			<?
			echo getArticles($idArticle,$userId,'');
			?>
			
		<?
		}
		?>
		<div id="situation">
			<!-- Où l'utilisateur se trouve sur le site -->
		</div>
	<?
	include('./include/aside.php');
	?>
</div>

<?php
include('./include/footer.php');
?>