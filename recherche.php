<?php
include('include/header.php');
include('include/haut.php');
?>

<div id="corps">
	<div class="contenu">
		<div class="texteBandeau">
			<span class='titre'>Résultat de la recherche</span>
		</div>
		<h1>
			<?php
			echo $_SESSION['dem_recherche'];
			?>
		</h1> 
		<div id="res_recherche">
			<?php
			echo $_SESSION['res_recherche'];
			?>
		</div>
	</div>
	<?
	include('include/aside.php');
	?>
</div>

<?php
include('include/footer.php');
?>