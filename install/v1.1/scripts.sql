ALTER TABLE  `nuxt_param` ADD  `param_version` VARCHAR( 3 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `nuxt_questions_categories` (
	`questions_categories_id` int(11) NOT NULL AUTO_INCREMENT,
	`questions_categories_libelle` VARCHAR(50) NOT NULL,
	`questions_categories_valide` int(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`questions_categories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ;

CREATE TABLE IF NOT EXISTS `nuxt_questions` (
  `questions_id` int(11) NOT NULL AUTO_INCREMENT,
  `questions_texte` longtext COLLATE latin1_general_ci NOT NULL,
  `questions_tags` VARCHAR(100),
  `questions_date_creation` date NOT NULL,
  `questions_valide` tinyint(1) DEFAULT '0',
  `questions_users_id_createur` int(11) NOT NULL,
  `questions_categories_id` int(11) NOT NULL,
  `questions_like` int(11) NOT NULL DEFAULT '0',
  `questions_unlike` int(11) NOT NULL DEFAULT '0',
  `questions_vues` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`questions_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ;

CREATE TABLE IF NOT EXISTS `nuxt_reponses` (
  `reponses_id` int(11) NOT NULL AUTO_INCREMENT,
  `reponses_texte` longtext COLLATE latin1_general_ci NOT NULL,
  `reponses_date_creation` date NOT NULL,
  `reponses_valide` tinyint(1) DEFAULT '0',
  `reponses_users_id_createur` int(11) NOT NULL,
  `reponses_like` int(11) NOT NULL DEFAULT '0',
  `reponses_unlike` int(11) NOT NULL DEFAULT '0',
  `questions_id` int(11) NOT NULL,
  PRIMARY KEY (`questions_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ;

CREATE TABLE IF NOT EXISTS `nuxt_lien_questions_users` (
  `l_questions_id` int(11) NOT NULL,
  `l_users_id` int(11) NOT NULL,
  `l_like` int(1) NOT NULL,
  `l_unlike` int(1) NOT NULL,
  PRIMARY KEY (`l_questions_id`,`l_users_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


INSERT INTO  `nuxt_modules` (  `modules_id` ,  `modules_libelle` ,  `modules_valide` ,  `modules_pages` ) 
VALUES (
'',  'Questions',  '1',  'questions.php'
);

UPDATE nuxt_param SET param_version = 'v1.1';