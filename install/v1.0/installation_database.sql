-- phpMyAdmin SQL Dump
-- version 2.6.4-pl3
-- http://www.phpmyadmin.net
-- 
-- Serveur: db472733173.db.1and1.com
-- Généré le : Samedi 10 Août 2013 à 12:11
-- Version du serveur: 5.1.67
-- Version de PHP: 5.3.3-7+squeeze16
-- 
-- Base de données: `db472733173`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_articles`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_articles` (
  `articles_id` int(11) NOT NULL AUTO_INCREMENT,
  `articles_titre` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `articles_texte` longtext COLLATE latin1_general_ci NOT NULL,
  `articles_date_creation` date NOT NULL,
  `articles_date_modification` date DEFAULT NULL,
  `articles_valide` tinyint(1) DEFAULT '0',
  `articles_users_id_createur` int(11) NOT NULL,
  `articles_users_id_valideur` int(11) DEFAULT NULL,
  `articles_tags` text COLLATE latin1_general_ci,
  `articles_image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `articles_categories_id` int(11) NOT NULL,
  `articles_like` int(11) NOT NULL DEFAULT '0',
  `articles_unlike` int(11) NOT NULL DEFAULT '0',
  `articles_vues` int(11) NOT NULL DEFAULT '0',
  `articles_com` int(11) NOT NULL DEFAULT '0',
  `articles_miseenavant` int(1) NOT NULL DEFAULT '0',
  `articles_upload_id` int(11) NOT NULL,
  PRIMARY KEY (`articles_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=10 ;

-- 
-- Contenu de la table `nuxt_articles`
-- 

INSERT INTO `nuxt_articles` (`articles_id`, `articles_titre`, `articles_texte`, `articles_date_creation`, `articles_date_modification`, `articles_valide`, `articles_users_id_createur`, `articles_users_id_valideur`, `articles_tags`, `articles_image`, `articles_categories_id`, `articles_like`, `articles_unlike`, `articles_vues`, `articles_com`, `articles_miseenavant`, `articles_upload_id`) 
VALUES (1, 'Bienvenue sur nuXt', 'nuXt est un CMS dÃ©veloppÃ© par l''auto entreprise pancard ([url]www.pancard.fr[/url]) pour ses clients.\nSur ce site, vous trouverez les dÃ©tails de ce CMS, ses fonctionnalitÃ©s, les templates existants, les modules Ã  venir, etc...\nSi vous avez des questions, n''hÃ©sitez pas Ã  les poser en contactant l''Ã©quipe support de nuXt ou directement pancard.\n\nEquipe Support Pancard :)\n[url]http://www.pancard.fr[/url]\n\n[img]http://nuxt.pancard.fr/admin/design/images/logo\\_nuxt\\_white.png[/img]\n[img]http://nuxt.pancard.fr/admin/design/images/logo\\_nuxt\\_red.png[/img]\n[img]http://nuxt.pancard.fr/admin/design/images/logo\\_nuxt\\_blue.png[/img]\n[img]http://nuxt.pancard.fr/admin/design/images/logo\\_nuxt\\_green.png[/img]\n', '2013-06-02', '2013-08-08', 1, 1, 1, 'nuxt,pancard', 'https://sphotos-b.xx.fbcdn.net/hphotos-ash3/941148_471505516252231_248358169_n.png', 1, 3, 0, 0, 0, 1, 2);
-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_articles_categories`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_articles_categories` (
  `articles_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `articles_categories_libelle` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `articles_categories_valide` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`articles_categories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

-- 
-- Contenu de la table `nuxt_articles_categories`
-- 

INSERT INTO `nuxt_articles_categories` (`articles_categories_id`, `articles_categories_libelle`, `articles_categories_valide`) 
VALUES (1, 'nuXt', 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_articles_commentaires`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_articles_commentaires` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `com_articles_id` int(11) NOT NULL,
  `com_texte` text COLLATE latin1_general_ci NOT NULL,
  `com_date_creation` date NOT NULL,
  `com_users_id` int(11) NOT NULL,
  `com_like` int(11) DEFAULT NULL,
  `com_unlike` int(11) DEFAULT NULL,
  `com_top` int(1) DEFAULT '0',
  PRIMARY KEY (`com_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `nuxt_articles_commentaires`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_design`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_design` (
  `design_id` int(11) NOT NULL AUTO_INCREMENT,
  `design_nom` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `design_chemin` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `design_logo_defaut` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `design_image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `design_valide` int(1) NOT NULL,
  PRIMARY KEY (`design_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Contenu de la table `nuxt_design`
-- 

INSERT INTO `nuxt_design` (`design_id`, `design_nom`, `design_chemin`, `design_logo_defaut`, `design_image`, `design_valide`) 
VALUES (1, 'Coquelicot', 'coquelicot', '/admin/design/images/logo_nuxt_red.png', '', 0),
(2, 'Sky', 'sky', '/admin/design/images/logo_nuxt_white.png', '', 0),
(3, 'nuXt', 'nuxt', '/admin/design/images/logo_nuxt.png', '', 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_droits`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_droits` (
  `droits_id` int(11) NOT NULL AUTO_INCREMENT,
  `droits_libelle` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `droits_valide` tinyint(1) NOT NULL DEFAULT '0',
  `droits_modules_id` int(11) NOT NULL,
  PRIMARY KEY (`droits_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

-- 
-- Contenu de la table `nuxt_droits`
-- 

INSERT INTO `nuxt_droits` (`droits_id`, `droits_libelle`, `droits_valide`, `droits_modules_id`) 
VALUES (1, 'AccÃ¨s Administration', 1, 0),
(2, 'AccÃ¨s Articles', 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_lien_articles_users`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_lien_articles_users` (
  `l_articles_id` int(11) NOT NULL,
  `l_users_id` int(11) NOT NULL,
  `l_like` int(1) NOT NULL,
  `l_unlike` int(1) NOT NULL,
  PRIMARY KEY (`l_articles_id`,`l_users_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Contenu de la table `nuxt_lien_articles_users`
-- 

INSERT INTO `nuxt_lien_articles_users` (`l_articles_id`, `l_users_id`, `l_like`, `l_unlike`) 
VALUES (1, -1, 1, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_lien_com_users`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_lien_com_users` (
  `l_com_id` int(11) NOT NULL,
  `l_users_id` int(11) NOT NULL,
  `l_like` int(11) NOT NULL,
  `l_unlike` int(11) NOT NULL,
  PRIMARY KEY (`l_com_id`,`l_users_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Contenu de la table `nuxt_lien_com_users`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_lien_droits_profils`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_lien_droits_profils` (
  `l_droits_id` int(11) NOT NULL AUTO_INCREMENT,
  `l_profils_id` int(11) NOT NULL,
  `l_actif` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`l_droits_id`,`l_profils_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

-- 
-- Contenu de la table `nuxt_lien_droits_profils`
-- 

INSERT INTO `nuxt_lien_droits_profils` (`l_droits_id`, `l_profils_id`, `l_actif`) 
VALUES (1, -1, 1),
(2, -1, 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_menus`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_menus` (
  `menus_id` int(11) NOT NULL AUTO_INCREMENT,
  `menus_nom` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menus_valide` tinyint(1) NOT NULL DEFAULT '0',
  `menus_modules_id` int(11) NOT NULL,
  PRIMARY KEY (`menus_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

-- 
-- Contenu de la table `nuxt_menus`
-- 

INSERT INTO `nuxt_menus` (`menus_id`, `menus_nom`, `menus_valide`, `menus_modules_id`) VALUES (1, 'Accueil', 1, 0),
(2, 'ActualitÃ©s', 1, 2);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_modules`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_modules` (
  `modules_id` int(11) NOT NULL AUTO_INCREMENT,
  `modules_libelle` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `modules_valide` int(1) NOT NULL,
  `modules_pages` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`modules_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

-- 
-- Contenu de la table `nuxt_modules`
-- 

INSERT INTO `nuxt_modules` (`modules_id`, `modules_libelle`, `modules_valide`, `modules_pages`) 
VALUES (1, 'Administration', 1, '/admin/'),
(2, 'Articles', 1, 'articles.php'),
(3, 'Utilisateurs', 1, 'users.php'),
(0, 'Home', 1, 'index.php');

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_pages`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_pages` (
  `pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `pages_titre` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pages_texte` longtext COLLATE latin1_general_ci,
  `pages_url` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pages_valide` tinyint(1) NOT NULL DEFAULT '0',
  `pages_modules_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- 
-- Contenu de la table `nuxt_pages`
-- 
-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_param`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_param` (
  `param_nom_site` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `param_slogan_site` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `param_description_site` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `param_mail_contact_site` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `param_url_site` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `param_design_id_site` int(1) DEFAULT NULL,
  `param_upload_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Contenu de la table `nuxt_param`
-- 

INSERT INTO `nuxt_param` (`param_nom_site`, `param_slogan_site`, `param_description_site`, `param_mail_contact_site`, `param_url_site`, `param_design_id_site`, `param_upload_id`) 
VALUES ('Votre Site', 'Votre Slogan', 'Votre Description', 'votre@mail.com', 'http://votreurl.fr', 3, 0);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_profils`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_profils` (
  `profils_id` int(11) NOT NULL AUTO_INCREMENT,
  `profils_libelle` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `profils_valide` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`profils_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci PACK_KEYS=1 AUTO_INCREMENT=8 ;

-- 
-- Contenu de la table `nuxt_profils`
-- 

INSERT INTO `nuxt_profils` (`profils_id`, `profils_libelle`, `profils_valide`) VALUES (-1, 'Super Administrateur', 1),
(1, 'Administrateur', 1),
(3, 'Membre', 1),
(2, 'ModÃ©rateur', 0),
(0, 'Inconnu', 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_upload`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_upload` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_nom` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `upload_chemin` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `upload_type` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `upload_taille` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `upload_valide` int(1) NOT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- 
-- Contenu de la table `nuxt_upload`
-- 

INSERT INTO `nuxt_upload` (`upload_id`, `upload_nom`, `upload_chemin`, `upload_type`, `upload_taille`, `upload_valide`) 
VALUES (2, 'welcome.jpg', 'welcome.jpg', 'image/jpeg', '445060', 1);

-- --------------------------------------------------------

-- 
-- Structure de la table `nuxt_users`
-- 

CREATE TABLE IF NOT EXISTS `nuxt_users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_login` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `users_password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `users_mail` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `users_nom` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `users_prenom` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `users_portable` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `users_fixe` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `users_avatar` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `users_date_creation` date NOT NULL,
  `users_facebook` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `users_twitter` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `users_site_web` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `users_valide` tinyint(1) NOT NULL DEFAULT '0',
  `users_profil_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`users_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- 
-- Contenu de la table `nuxt_users`
-- 

INSERT INTO `nuxt_users` (`users_id`, `users_login`, `users_password`, `users_mail`, `users_nom`, `users_prenom`, `users_portable`, `users_fixe`, `users_avatar`, `users_date_creation`, `users_facebook`, `users_twitter`, `users_site_web`, `users_valide`, `users_profil_id`) 
VALUES (-1, 'peteralexief', 'peteralexief', 'support@pancard.fr', NULL, NULL, NULL, NULL, NULL, '2013-06-02', NULL, NULL, 'www.pancard.fr', 1, -1),
(1, 'Administrateur', 'aDmIn54', 'votre@mail.fr', '', '', '', '', 'admin.png', '2013-06-02', '', '', 'www.votreurl.fr', 1, 1);